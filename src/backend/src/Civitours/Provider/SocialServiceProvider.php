<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.05.18
 * Time: 13:33
 */

namespace Civitours\Provider;


use Civitours\Service\SocialService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class SocialServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['social.service'] = function () use ($app) {
            return new SocialService(
                $app['social.google.clientId'],
                $app['social.facebook.appid'],
                $app['social.facebook.secret']
            );
        };
    }
}
