<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.18
 * Time: 13:13
 */

namespace Civitours\Provider;

use Civitours\Service\GeoDataService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class GeoDataServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['geo_data.service'] = function () use ($app) {
            return new GeoDataService($app['db'], $app['upload.service']);
        };
    }
}
