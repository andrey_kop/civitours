<?php
namespace Civitours\Provider;

use Civitours\Service\UserService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;


class UserServiceProvider implements ServiceProviderInterface
{
    const CONFIRMATION_ROUTE = 'confirm';

    public function register(Container $app)
    {
        $app['user.service'] = function () use ($app) {
            return new UserService(
                $app['db'],
                $app['mail.service'],
                $app['api.url'] . DIRECTORY_SEPARATOR . self::CONFIRMATION_ROUTE,
                $app['base.url'] . '?modal=newPassword&code='
            );
        };
    }
}