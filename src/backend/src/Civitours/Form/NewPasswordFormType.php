<?php
namespace Civitours\Form;

use Civitours\Entity\NewPasswordData;
use Civitours\Service\UserService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Form type for user registration
 *
 * Class RegistrationFormType
 * @package Civitours\Form
 */
class NewPasswordFormType extends AbstractType
{
    /**
     * Construct the form with assertions
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', PasswordType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Regex([
                        'pattern' => '/^(?=.*[A-Z]).{8,}$/',
                        'message' => 'Password should contain at least one uppercase symbol and consist at least of 8 symbols'
                    ])
                ]
            ])
            ->add('passwordConfirm', PasswordType::class, [
                'constraints' => [
                    new Assert\NotBlank()
                ]
            ])
            ->add('token', PasswordType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Callback([
                        'callback'  => [$this, 'validateUserResetToken'],
                        'payload'   => $options['user_service']
                    ])
                ]
            ]);
    }

    /**
     * Check passwords are identical
     *
     * @param NewPasswordData $data
     * @param ExecutionContextInterface $context
     */
    public function validatePassword(NewPasswordData $data, ExecutionContextInterface $context) {
        if($data->password !== $data->passwordConfirm) {
            $context->buildViolation('Password and confirm should match')
                ->atPath('passwordConfirm')
                ->addViolation();
        }
    }

    /**
     * Validate that there is user with such validation token
     *
     * @param $data
     * @param ExecutionContextInterface $context
     * @param UserService $payload
     */
    public function validateUserResetToken($data, ExecutionContextInterface $context, $payload) {
        if(false === $payload->getUserByRecoveryToken($data)) {
            $context->buildViolation('Wrong token provided')
                ->atPath('token')
                ->addViolation();
        }
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => NewPasswordData::class,
            'constraints'       => [
                new Assert\Callback([$this, 'validatePassword']),
            ],
            'csrf_protection'   => false,
        ));

        $resolver->setRequired(['user_service']);
    }
}