<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.05.18
 * Time: 13:10
 */

namespace Civitours\Form;


use Civitours\Entity\SocialLoginData;

use Civitours\Service\SocialService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


class SocialLoginType extends AbstractType
{
    /**
     * @var SocialService
     */
    private $socialService;

    /**
     * Construct the form with assertions
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->socialService = $options['social_service'];

        $builder
            ->add('id', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('name', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('image', TextType::class)
            ->add('email', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('provider', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('idToken', TextType::class)
            ->add('token', TextType::class);
    }

    /**
     * Check token is valid
     *
     * @param SocialLoginData $data
     * @param ExecutionContextInterface $context
     */
    public function validateToken(SocialLoginData $data, ExecutionContextInterface $context) {
        if(false === $this->socialService->validateToken($data)) {
            $context->buildViolation('Wrong token from social login')
                ->atPath('token')
                ->addViolation();
        }
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => SocialLoginData::class,
            'constraints'       => [
                new Assert\Callback([$this, 'validateToken']),
            ],
            'csrf_protection'   => false,
        ));

        $resolver->setRequired(['social_service']);
    }
}
