<?php

namespace Civitours\Service;

use Civitours\Entity\ActivityData;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ActivityService
{

    /**
     *  Database connection
     *
     * @var Connection
     */
    private $db = null;

    /**
     * Url to images directory
     *
     * @var string
     */
    private $urlToImages = '';

    /**
     * @var UploadService
     */
    private $uploadService;

    /**
     * UserService constructor.
     *
     * @param Connection $db
     * @param $urlToImages
     * @param UploadService $uploadService
     */
    public function __construct(Connection $db, $urlToImages, UploadService $uploadService)
    {
        $this->db = $db;
        $this->urlToImages = $urlToImages;
        $this->uploadService = $uploadService;
    }

    /**
     * Get list of countries with activities available
     *
     * @param bool $excludeDraft
     * @return array
     */
    public function getCountriesList($excludeDraft = true) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_countries.id',
                't_countries.code',
                't_countries.name',
                't_countries.currency',
                't_countries.calling_code',
                't_countries.languages',
                't_countries.locale_name',
                't_countries.avatar'
            )
            ->from('activities', 't_activities')
            ->innerJoin('t_activities','countries','t_countries', 't_countries.id = t_activities.country')
            ->groupBy(
                't_countries.id',
                't_countries.code',
                't_countries.name',
                't_countries.currency',
                't_countries.calling_code',
                't_countries.languages',
                't_countries.locale_name',
                't_countries.avatar'
            );

        if ($excludeDraft) {
            $queryBuilder->andWhere('t_activities.is_draft = false');
        }

        return array_map([$this, 'extractLocaleName'], array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll()));
    }

    /**
     * Get list of countries with activities available
     *
     * @param $countryCode
     * @param bool $excludeDraft
     * @return array
     */
    public function getCityList($countryCode, $excludeDraft = true) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_cities.id',
                't_cities.country',
                't_cities.name',
                't_cities.avatar',
                't_cities.offset',
                't_cities.locale_name'
            )
            ->from('activities', 't_activities')
            ->innerJoin('t_activities','cities','t_cities', 't_cities.id = t_activities.city')
            ->innerJoin('t_activities','countries','t_countries', 't_countries.id = t_activities.country')
            ->groupBy(
                't_cities.id',
                't_cities.country',
                't_cities.name',
                't_cities.avatar',
                't_cities.offset',
                't_cities.locale_name'
            );

        if (intval($countryCode)) {
            $queryBuilder
                ->where('t_countries.id = ?')
                ->setParameter(0, intval($countryCode));
        } else {
            $queryBuilder
                ->where('t_countries.code = ?')
                ->setParameter(0, $countryCode);
        }

        if ($excludeDraft) {
            $queryBuilder->andWhere('t_activities.is_draft = false');
        }

        return array_map([$this, 'extractLocaleName'], array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll()));
    }

    /**
     * Get country info with activities
     *
     * @param $countryCode
     * @return mixed
     */
    public function getCountryInfo($countryCode) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_countries.id',
                't_countries.code',
                't_countries.name',
                't_countries.currency',
                't_countries.calling_code',
                't_countries.locale_name',
                't_countries.languages',
                't_countries.avatar',
                'count(a.id) as excursions',
                'count(r.id) as reviews',
                'coalesce(round(avg(r.rate), 1), 0) as rate',
                '45 as travelers' // TODO: correct travelers retrieving
            )
            ->from('countries', 't_countries')
            ->leftJoin('t_countries','activities','a', 't_countries.id = a.country')
            ->leftJoin('a','reviews','r', 'a.id = r.activity')
            ->where('t_countries.code = ?')
            ->setParameter(0, $countryCode)
            ->groupBy(
                't_countries.id',
                't_countries.code',
                't_countries.name',
                't_countries.currency',
                't_countries.calling_code',
                't_countries.locale_name',
                't_countries.languages',
                't_countries.avatar'
            );

        return $this->extractLocaleName($this->populateAvatar($queryBuilder->execute()->fetch()));
    }

    /**
     * Get list of country destinations
     *
     * @param $countryCode
     * @param null $sort
     * @return array
     */
    public function getDestinations($countryCode = null, $sort = null) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                'ct.id',
                'ct.country',
                'ct.name',
                'ct.avatar',
                'ct.locale_name',
                'ct.offset',
                'c.name as country_name',
                'count(r.id) as reviews',
                'coalesce(round(avg(r.rate), 1), 0) as rate',
                'count (a.id) as excursions',
                '9.2 as rate_us', // TODO: calculate rate us
                '45 as travelers' // TODO: correct travelers retrieving
            )
            ->from('countries', 'c')
            ->innerJoin('c','activities','a', 'c.id = a.country')
            ->innerJoin('a','cities','ct', 'ct.id = a.city')
            ->leftJoin('a','reviews','r', 'a.id = r.activity')
            ->groupBy(
                'ct.id',
                'ct.country',
                'ct.name',
                'ct.avatar',
                'ct.locale_name',
                'ct.offset',
                'c.name'
            );

        if ($countryCode) {
            $queryBuilder
                ->where('c.code = ?')
                ->setParameter(0, $countryCode);
        }
        if ($sort) {
            $queryBuilder
                ->orderBy($sort, 'DESC');
        }

        return array_map([$this, 'extractLocaleName'], array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll()));
    }

    /**
     * Retrieve destination info
     *
     * @param $idDestination
     * @return mixed
     */
    public function getDestination($idDestination) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                'c.id',
                'c.country',
                'c.name',
                'c.avatar',
                'c.locale_name',
                'c.offset',
                'co.name as country_name',
                'count(r.id) as reviews',
                'coalesce(round(avg(r.rate), 1), 0) as rate',
                'count (a.id) as excursions',
                '9.2 as rate_us', // TODO: calculate rate us
                '45 as travelers' // TODO: correct travelers retrieving
            )
            ->from('cities', 'c')
            ->leftJoin('c','activities','a', 'c.id = a.city')
            ->leftJoin('c','countries','co', 'co.id = c.country')
            ->leftJoin('a','reviews','r', 'a.id = r.activity')
            ->where('c.id = ?')
            ->setParameter(0, $idDestination)
            ->setMaxResults(1)
            ->groupBy(
                'c.id',
                'c.country',
                'c.name',
                'c.avatar',
                'c.locale_name',
                'c.offset',
                'co.name'
            );

        $stmt = $queryBuilder->execute();
        if (!$stmt->rowCount()) {
            throw new NotFoundHttpException('Wrong city code is provided');
        }
        return $this->extractLocaleName($this->populateAvatar($stmt->fetch()));
    }

    /**
     * Get base query builder for activities
     *
     * @param bool $excludeDraft
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function getPreparedActivityQueryBuilder($excludeDraft = true) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_activities.id',
                't_activities.name',
                't_activities.avatar',
                't_activities.short_description',
                't_activities.language',
                't_activities.duration',
                't_activities.category',
                't_activities.is_draft',
                'coalesce(min(CASE WHEN t_person_types.id is null THEN null ELSE t_activity_prices.price END), 0) AS min_price',
                'count(t_reviews.id) AS reviews',
                'coalesce(avg(t_reviews.rate), 0) AS rate'
            )
            ->from('activities', 't_activities')
            ->leftJoin('t_activities','activity_prices','t_activity_prices', 't_activity_prices.activity = t_activities.id')
            ->leftJoin('t_activities','reviews','t_reviews', 't_activities.id = t_reviews.activity')
            ->leftJoin('t_activity_prices', 'person_types', 't_person_types', 't_person_types.id = t_activity_prices.person_type AND t_person_types.is_adult IS TRUE')
            ->groupBy(
                't_activities.id',
                't_activities.name',
                't_activities.avatar',
                't_activities.short_description',
                't_activities.language',
                't_activities.duration',
                't_activities.category',
                't_activities.is_draft'
            );

        if ($excludeDraft) {
            $queryBuilder->andWhere('t_activities.is_draft = false');
        }

        return $queryBuilder;
    }

    /**
     * Get full activity list
     *
     * @param bool $excludeDraft
     * @return array
     */
    public function getList($excludeDraft = true) {
        $queryBuilder = $this->getPreparedActivityQueryBuilder($excludeDraft);
        $queryBuilder->orderBy('t_activities.id', 'asc');
        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get featured activities list
     *
     * @param int $offset
     * @param int $limit
     * @param null $country
     * @return array
     */
    public function getFeaturedActivities($offset = 0, $limit = 6, $country = null) {
        $queryBuilder = $this->getPreparedActivityQueryBuilder()
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        if($country) {
            $queryBuilder
                ->innerJoin('t_activities','countries','t_countries', 't_countries.id=t_activities.country')
                ->where('t_countries.code = ?')
                ->setParameter(0, $country);
        }
        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get activity short list for city
     *
     * @param $cityId
     * @param array $activities
     * @return array
     */
    public function getActivitiesForCity($cityId, $activities = null) {
        $queryBuilder = $this->getPreparedActivityQueryBuilder()
            ->where('city = :city')
            ->setParameter('city', $cityId);

        if(null !== $activities) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('t_activities.id', $activities));
        }
        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Retrieve activity by id
     *
     * @param $id
     * @param bool $excludeDraft
     * @return array|false
     */
    public function getActivity($id, $excludeDraft = true) {

        $queryBuilder = $this->getPreparedActivityQueryBuilder()
            ->addSelect(
                't_activities.country',
                't_countries.name as country_name',
                't_activities.city',
                't_cities.name as city_name',
                't_activities.description',
                't_activities.included',
                't_activities.not_included',
                't_activities.when_to_book',
                't_activities.acessibility',
                't_activities.ticket',
                't_activities.how_to_book',
                't_activities.meeting_point_latitude',
                't_activities.meeting_point_longitude',
                't_activities.meeting_point_text',
                't_activities.cancelation_hours_before',
                't_activities.cancelation_descriprion'
            )
            ->leftJoin('t_activities','countries','t_countries', 't_countries.id = t_activities.country')
            ->leftJoin('t_activities','cities','t_cities', 't_cities.id = t_activities.city')
            ->where('t_activities.id = ?')
            ->setMaxResults(1)
            ->setParameter(0, $id)
            ->addGroupBy(
                't_activities.country',
                't_countries.name',
                't_activities.city',
                't_cities.name',
                't_activities.description',
                't_activities.included',
                't_activities.not_included',
                't_activities.when_to_book',
                't_activities.acessibility',
                't_activities.ticket',
                't_activities.how_to_book',
                't_activities.meeting_point_latitude',
                't_activities.meeting_point_longitude',
                't_activities.meeting_point_text',
                't_activities.cancelation_hours_before',
                't_activities.cancelation_descriprion'
            );

        if ($excludeDraft) {
            $queryBuilder->andWhere('t_activities.is_draft = false');
        }

        return $this->populateAvatar($queryBuilder->execute()->fetch());
    }

    /**
     * Retrieve related activities
     *
     * @param $idActivity
     * @param $limit
     * @return array
     */
    public function getRelatedActivities($idActivity, $limit = 3) {
        $activityInfo = $this->getActivity($idActivity);
        if ($activityInfo) {
            $city = $activityInfo['city'];
            $queryBuilder = $this->getPreparedActivityQueryBuilder();
            $queryBuilder
                ->leftJoin('t_activities', 'order_activities', 't_order_activities', 't_activities.id = t_order_activities.activity')
                ->setMaxResults($limit)
                ->where($queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('t_activities.city', '?'),
                    $queryBuilder->expr()->neq('t_activities.id', '?')
                ))
                ->setParameter(0, $city)
                ->setParameter(1, $idActivity)
                ->orderBy('count(t_order_activities.id)', 'DESC');
            return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
        } else {
            return [];
        }
    }

    /**
     * Get activity schedule
     *
     * @param $idActivity
     * @return mixed|null
     */
    public function getActivitySchedule($idActivity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('t_activity_schedule.time', 't_cities.offset')
            ->from('activity_schedule', 't_activity_schedule')
            ->innerJoin('t_activity_schedule', 'activities', 't_activities', 't_activities.id=t_activity_schedule.activity')
            ->innerJoin('t_activities', 'cities', 't_cities', 't_cities.id=t_activities.city')
            ->where('activity = ?')
            ->setParameter(0, $idActivity)
            ->setMaxResults(1);

        $result = $queryBuilder->execute()->fetch();
        if (!$result) {
            return null;
        }
        return $this->extractSchedule($result);
    }

    /**
     * Retrieve all available prices and types for activity
     *
     * @param $idActivity
     * @return array
     */
    public function getTypes($idActivity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_activity_prices.price',
                't_activity_types.id as activity_type_id',
                't_activity_types.name as activity_type',
                't_person_types.id as person_type_id',
                't_person_types.name as person_type_name',
                't_person_types.is_adult as is_adult'
            )
            ->from('activity_prices', 't_activity_prices')
            ->innerJoin('t_activity_prices','activity_types','t_activity_types', 't_activity_types.id = t_activity_prices.activity_type')
            ->innerJoin('t_activity_prices','person_types','t_person_types', 't_person_types.id = t_activity_prices.person_type')
            ->where('t_activity_prices.activity = ?')
            ->orderBy('t_person_types.id', 'ASC')
            ->setParameter(0, $idActivity);

        $dbResult = $queryBuilder->execute()->fetchAll();
        $activityTypes = [];

        //Build tree from prices
        // activityType -> prices
        foreach ($dbResult as $row) {
            $activityTypeId = $row['activity_type_id'];
            $personTypeId = $row['person_type_id'];

            if (empty($activityTypes[$activityTypeId])) {
                $activityTypes[$activityTypeId] = [
                    'id'        => $activityTypeId,
                    'name'      => $row['activity_type'],
                    'tickets'   => []
                ];
            }
            $activityTypes[$row['activity_type_id']]['tickets'][$personTypeId] = [
                'id'        => $personTypeId,
                'name'      => $row['person_type_name'],
                'price'     => $row['price'],
                'is_adult'  => $row['is_adult']
            ];
        }

        // Convert to plain array
        $activityTypes = array_values($activityTypes);
        foreach ($activityTypes as &$activityType) {
            $activityType['tickets'] = array_values($activityType['tickets']);
        }
        return $activityTypes;
    }

    /**
     * Get list of activity categories
     *
     * @return array
     */
    public function getCategoriesList() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('categories');
        return $queryBuilder->execute()->fetchAll();
    }


    public function searchByDatesAndTime($idCity, $dates, $times) {

        $this->db->query("SET TIME ZONE 'UTC'");

        $explodeItem = function ($item) {
            return array_map('intval', explode(':', $item));
        };

        $dates = array_map($explodeItem, $dates);
        $times = array_map($explodeItem, $times);

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('activity')
            ->from('activity_schedule');

        $dateTimesExpStr = [];
        if (!empty($dates) && !empty($times)) {
            // In this case we should filter simultaneously
            foreach ($dates as $date) {
                list ($fromDate, $toDate) = $date;
                foreach ($times as $time) {
                    list ($fromTime, $toTime) = $time;
                    // TODO: if from is less than to case
                    $dateTimesExpStr[] = "'[${fromTime},${toTime}]'::int4range @> ANY(extract_hour_for_day(time, '[${fromDate},${toDate}]'::int4range))";
                }
            }
            $queryBuilder->where($queryBuilder->expr()->orX()->addMultiple($dateTimesExpStr));
        } else {
            // Otherwise we filter on date or on time
            $datesExpStr = [];
            foreach ($dates as $date) {
                list ($from, $to) = $date;
                $from = intval($from);
                $to = intval($to);
                $datesExpStr[] = "int4range(${from}, ${to}) @> ANY(time)";
            }

            $timesExpStr = [];
            foreach ($times as $time) {
                list ($from, $to) = $time;
                $from = intval($from);
                $to = intval($to);
                // TODO: if from is less than to case
                $timesExpStr[] = "int4range(${from}, ${to}) @> ANY(extract_hour(time))";
            }
            $dateExpr = $queryBuilder->expr()->orX()->addMultiple($datesExpStr);
            $timeExpr = $queryBuilder->expr()->orX()->addMultiple($timesExpStr);
            $queryBuilder->where($queryBuilder->expr()->andX($dateExpr, $timeExpr));
        }

//        echo $queryBuilder; exit;

        $stmt = $queryBuilder->execute();
        $activities = [];
        while ($idActivity = $stmt->fetchColumn()) {
            array_push($activities, $idActivity);
        };
        if(empty($activities)) {
            // No need to load activities
            return [];
        }

        return $this->getActivitiesForCity($idCity, $activities);
    }

    /**
     * Get activity gallery items
     *
     * @param $idActivity
     * @return array
     */
    public function getGallery($idActivity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                'path',
                'description',
                'is_main'
            )
            ->from('gallery', 'a')
            ->where('activity = ?')
            ->setParameter(0, $idActivity);

        return array_map([$this, 'populatePath'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Set draft status for activity
     *
     * @param $idActivity
     * @param $draftStatus
     * @return int
     */
    public function setDraftStatus($idActivity, $draftStatus) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('activities')
            ->set('is_draft', ':is_draft')
            ->where('id = :id')
            ->setParameter('is_draft', $draftStatus, ParameterType::BOOLEAN)
            ->setParameter('id', $idActivity);

        return $queryBuilder->execute();
    }

    /**
     * Create new activity
     *
     * @param ActivityData $data
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function createActivity(ActivityData $data) {

        $this->db->beginTransaction();
        try {
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->insert('activities')
                ->setValue('country'                   , ':country')
                ->setValue('city'                      , ':city')
                ->setValue('category'                  , ':category')
                ->setValue('name'                      , ':name')
                ->setValue('short_description'         , ':short')
                ->setValue('description'               , ':description')
                ->setValue('duration'                  , ':duration')
                ->setValue('language'                  , ':language')
                ->setValue('included'                  , ':included')
                ->setValue('not_included'              , ':not_included')
                ->setValue('when_to_book'              , ':when_to_book')
                ->setValue('acessibility'              , ':access')
                ->setValue('ticket'                    , ':ticket')
                ->setValue('how_to_book'               , ':how')
                ->setValue('meeting_point_latitude'    , ':lat')
                ->setValue('meeting_point_longitude'   , ':lon')
                ->setValue('meeting_point_text'        , ':text')
                ->setValue('cancelation_hours_before'  , ':hours')
                ->setValue('cancelation_descriprion'   , ':cancel')
                ->setParameters([
                    'country'      => $data->country,
                    'city'         => $data->city,
                    'category'     => $data->category,
                    'name'         => $data->name,
                    'short'        => $data->shortDescription,
                    'description'  => $data->description,
                    'duration'     => $data->duration,
                    'language'     => $data->language,
                    'included'     => $data->included,
                    'not_included' => $data->notIncluded,
                    'when_to_book' => $data->whenToBook,
                    'access'       => $data->accessibility,
                    'ticket'       => $data->ticket,
                    'how'          => $data->howToBook,
                    'lat'          => $data->meetingPointLatitude,
                    'lon'          => $data->meetingPointLongitude,
                    'text'         => $data->meetingPointText,
                    'hours'        => $data->cancelationHoursBefore,
                    'cancel'       => $data->cancelationDescription
                ]);
            $queryBuilder->execute();

            $idActivity = $this->db->lastInsertId();
            if (!empty($data->avatar)) {
                $activityInfo = $this->getActivity($idActivity, false);
                $data->avatar = $this->uploadService->copyActivityImage(
                    $data->avatar,
                    $activityInfo['country_name'],
                    $activityInfo['city_name'],
                    $idActivity
                );
                $queryBuilder
                    ->update('activities')
                    ->set('avatar', ':avatar')
                    ->where('id = :id')
                    ->setParameter('avatar', $data->avatar)
                    ->setParameter('id', $idActivity);
                $queryBuilder->execute();
            }
            $this->saveActivityTypes($idActivity, $data->types);
            $this->saveActivityGallery($idActivity, $data->gallery);
            $this->saveActivitySchedule($idActivity, $data->schedule);
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * Update activity with provided data
     *
     * @param $idActivity
     * @param ActivityData $data
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function updateActivity($idActivity, ActivityData $data) {

        $this->db->beginTransaction();
        try {
            if (!empty($data->avatar)) {
                $activityInfo = $this->getActivity($idActivity, false);
                $data->avatar = $this->uploadService->copyActivityImage(
                    $data->avatar,
                    $activityInfo['country_name'],
                    $activityInfo['city_name'],
                    $idActivity
                );
            }

            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->update('activities')
                ->where('id = :id')
                ->set('country'                   , ':country')
                ->set('city'                      , ':city')
                ->set('category'                  , ':category')
                ->set('name'                      , ':name')
                ->set('short_description'         , ':short')
                ->set('description'               , ':description')
                ->set('duration'                  , ':duration')
                ->set('language'                  , ':language')
                ->set('included'                  , ':included')
                ->set('not_included'              , ':not_included')
                ->set('when_to_book'              , ':when_to_book')
                ->set('acessibility'              , ':access')
                ->set('ticket'                    , ':ticket')
                ->set('how_to_book'               , ':how')
                ->set('meeting_point_latitude'    , ':lat')
                ->set('meeting_point_longitude'   , ':lon')
                ->set('meeting_point_text'        , ':text')
                ->set('cancelation_hours_before'  , ':hours')
                ->set('cancelation_descriprion'   , ':cancel')
                ->setParameters([
                    'country'      => $data->country,
                    'city'         => $data->city,
                    'category'     => $data->category,
                    'name'         => $data->name,
                    'short'        => $data->shortDescription,
                    'description'  => $data->description,
                    'duration'     => $data->duration,
                    'language'     => $data->language,
                    'included'     => $data->included,
                    'not_included' => $data->notIncluded,
                    'when_to_book' => $data->whenToBook,
                    'access'       => $data->accessibility,
                    'ticket'       => $data->ticket,
                    'how'          => $data->howToBook,
                    'lat'          => $data->meetingPointLatitude,
                    'lon'          => $data->meetingPointLongitude,
                    'text'         => $data->meetingPointText,
                    'hours'        => $data->cancelationHoursBefore,
                    'cancel'       => $data->cancelationDescription,
                    'id'           => $idActivity
                ]);

            if(!empty($data->avatar)) {
                $queryBuilder->set('avatar', ':avatar')->setParameter(':avatar', $data->avatar);
            }
            $queryBuilder->execute();
            $this->saveActivityTypes($idActivity, $data->types);
            $this->saveActivityGallery($idActivity, $data->gallery);
            $this->saveActivitySchedule($idActivity, $data->schedule);
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }

    }

    /**
     * Save types for activity
     *
     * @param $idActivity
     * @param $types
     */
    private function saveActivityTypes($idActivity, $types) {
        // Clear prices for activity
        $qb = $this->db->createQueryBuilder();
        $qb->delete('activity_prices')
           ->where('activity = ?')
           ->setParameter(0, $idActivity);
        $qb->execute();

        $ticketRef = [];
        foreach ($types as $type) {
            if (empty($type['id'])) {
                $type['id'] = $this->createNewActivityType($idActivity, $type['name']);
            } else {
                $this->updateActivityType($type['id'], $type['name']);
            }
            foreach ($type['tickets'] as $index => $ticket) {
                if (empty($ticketRef[$index])) {
                    if (empty($ticket['id'])) {
                        $ticket['id'] = $this->createNewPersonType($idActivity, $ticket['name'], $ticket['is_adult']);
                    } else {
                        $this->updatePersonType($ticket['id'], $ticket['name'], $ticket['is_adult']);
                    }
                    $ticketRef[$index] = $ticket;
                }
                $qb = $this->db->createQueryBuilder();
                $qb->insert('activity_prices')
                    ->setValue('activity', ':activity')
                    ->setValue('person_type', ':person')
                    ->setValue('activity_type', ':type')
                    ->setValue('price', ':price')
                    ->setParameters([
                        'activity'  => $idActivity,
                        'person'    => $ticketRef[$index]['id'],
                        'type'      => $type['id'],
                        'price'     => $ticket['price']
                    ]);
                $qb->execute();
            }
        }
    }

    /**
     * Create new activity type and return it`s id
     *
     * @param $idActivity
     * @param $name
     * @return string
     */
    private function createNewActivityType($idActivity, $name) {
        $qb = $this->db->createQueryBuilder();
        $qb->insert('activity_types')
            ->setValue('activity', ':activity')
            ->setValue('name', ':name')
            ->setParameters([
                'activity' => $idActivity,
                'name'     => $name
            ]);
        $qb->execute();
        return $this->db->lastInsertId();
    }

    /**
     * Create new activity type and return it`s id
     *
     * @param $id
     * @param $name
     * @return string
     */
    private function updateActivityType($id, $name) {
        $qb = $this->db->createQueryBuilder();
        $qb->update('activity_types')
            ->set('name', ':name')
            ->where('id = :id')
            ->setParameters([
                'id'    => $id,
                'name'  => $name
            ]);
        return $qb->execute();
    }

    /**
     * Create new person type and return it`s id
     *
     * @param $idActivity
     * @param $name
     * @param $isAdult
     * @return string
     */
    private function createNewPersonType($idActivity, $name, $isAdult) {
        $qb = $this->db->createQueryBuilder();
        $qb->insert('person_types')
            ->setValue('activity', ':activity')
            ->setValue('name', ':name')
            ->setValue('is_adult', ':adult')
            ->setParameters([
                'activity' => $idActivity,
                'name'     => $name,
            ])
            ->setParameter('adult', $isAdult, ParameterType::BOOLEAN);
        $qb->execute();
        return $this->db->lastInsertId();
    }

    /**
     * Update person type name
     *
     * @param $id
     * @param $name
     * @param $isAdult
     * @return string
     */
    private function updatePersonType($id, $name, $isAdult) {
        $qb = $this->db->createQueryBuilder();
        $qb->update('person_types')
            ->set('name', ':name')
            ->set('is_adult', ':adult')
            ->where('id = :id')
            ->setParameters([
                'id'    => $id,
                'name'  => $name
            ])
            ->setParameter('adult', $isAdult, ParameterType::BOOLEAN);
        return $qb->execute();
    }

    /**
     * Save item gallery
     *
     * @param $idActivity
     * @param $gallery
     */
    private function saveActivityGallery($idActivity, $gallery) {
        // Clear gallery for activity
        $qb = $this->db->createQueryBuilder();
        $qb->delete('gallery')
            ->where('activity = ?')
            ->setParameter(0, $idActivity);
        $qb->execute();

        foreach ($gallery as $item) {
            $activityInfo = $this->getActivity($idActivity, false);
            if (!empty($item['upload'])) {
                $item['path'] = $this->uploadService->copyActivityImage(
                    $item['upload'],
                    $activityInfo['country_name'],
                    $activityInfo['city_name'],
                    $idActivity
                );
            } else {
                $item['path'] = str_replace($this->urlToImages, '', $item['path']);
            }
            $qb = $this->db->createQueryBuilder();
            $qb->insert('gallery')
                ->setValue('activity', ':activity')
                ->setValue('path', ':path')
                ->setValue('description', ':description')
                ->setValue('is_main', ':is_main')
                ->setParameter('activity', $idActivity)
                ->setParameter('path', $item['path'])
                ->setParameter('description', $item['description'])
                ->setParameter('is_main', $item['is_main'], ParameterType::BOOLEAN);
            $qb->execute();
        }
    }

    /**
     * Save activity schedule
     *
     * @param $idActivity
     * @param $schedule
     */
    private function saveActivitySchedule($idActivity, $schedule) {
        // Clear shcedule for activity
        $qb = $this->db->createQueryBuilder();
        $qb->delete('activity_schedule')
            ->where('activity = ?')
            ->setParameter(0, $idActivity);
        $qb->execute();

        $qb = $this->db->createQueryBuilder();
        $times = $schedule['time'];
        $qb->insert('activity_schedule')
           ->setValue('activity', ':activity')
           ->setValue('time', ':time')
            ->setParameter('activity', $idActivity)
            ->setParameter('time', '{' . implode(',', $times) . '}');
        $qb->execute();
    }

    /**
     * Callback to fulfill image url
     *
     * @param $itemData
     * @return mixed
     */
    private function populateAvatar($itemData) {
        if(!empty($itemData['avatar'])) {
            $itemData['avatar'] = $this->urlToImages . $itemData['avatar'];
        }
        return $itemData;
    }

    /**
     * Callback to fulfill image url
     *
     * @param $itemData
     * @return mixed
     */
    private function populatePath($itemData) {
        $itemData['path'] = $this->urlToImages . $itemData['path'];
        return $itemData;
    }

    /**
     * Convert stored schedule to array
     *
     * @param $itemData
     * @return mixed
     */
    private function extractSchedule($itemData) {
        if(!empty($itemData['time'])) {
            $itemData['time'] = array_filter(explode(',', substr($itemData['time'], 1, -1)));
        }
        return $itemData;
    }

    /**
     * Convert locale name to db format
     *
     * @param $itemData
     * @return mixed
     */
    private function extractLocaleName($itemData) {
        if(!empty($itemData['locale_name'])) {
            $locales = explode(',', substr($itemData['locale_name'], 1, -1));
            $itemData['locale_name'] = []; // Convert from string to array
            foreach ($locales as $locale) {
                $locale = str_replace('"', '', $locale);
                list ($id, $name) = explode(':', $locale);
                $itemData['locale_name'][$id] = $name;
            }
        }
        return $itemData;
    }
}
