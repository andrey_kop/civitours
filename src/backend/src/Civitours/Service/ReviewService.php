<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.05.18
 * Time: 11:32
 */

namespace Civitours\Service;


use Doctrine\DBAL\Connection;

/**
 * Service to operate with reviews
 *
 * Class ReviewService
 * @package Civitours\Service
 */
class ReviewService
{
    /**
     * @var Connection
     */
    private $db;

    /**
     * ReviewService constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Retrieve reviews for city
     *
     * @param $idCity
     * @param int $limit
     * @return array
     */
    public function getReviewsForCity($idCity, $limit = 4) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('t_reviews.*')
            ->from('reviews', 't_reviews')
            ->innerJoin('t_reviews','activities','t_activities', 't_reviews.activity = t_activities.id')
            ->where('t_activities.city = ?')
            ->orderBy('created_at', 'DESC')
            ->setParameter(0, $idCity)
            ->setMaxResults($limit);

        return $queryBuilder->execute()->fetchAll();
    }


    /**
     * Get reviews for activity
     *
     * @param $idActivity
     * @param int $limit
     * @return array
     */
    public function getReviewsForActivity($idActivity, $limit = 6) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('t_reviews.*')
            ->from('reviews', 't_reviews')
            ->where('t_reviews.activity = ?')
            ->orderBy('created_at', 'DESC')
            ->setParameter(0, $idActivity)
            ->setMaxResults($limit);

        return $queryBuilder->execute()->fetchAll();
    }
}