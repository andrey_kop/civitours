<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.05.18
 * Time: 13:19
 */

namespace Civitours\Service;

use Civitours\Entity\SocialLoginData;
use Google_Client;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Service to validate data from social login
 *
 * Class SocialService
 * @package Civitours\Service
 */
class SocialService
{
    const GOOGLE_PROVIDER = 'google';
    const FACEBOOK_PROVIDER = 'facebook';

    private $googleClientId;
    private $facebookAppid;
    private $facebookSecret;

    public function __construct($googleClientId, $facebookAppid, $facebookSecret)
    {
        $this->googleClientId = $googleClientId;
        $this->facebookAppid = $facebookAppid;
        $this->facebookSecret = $facebookSecret;
    }

    /**
     * Perform user token validation
     *
     * @param SocialLoginData $data
     * @return bool
     */
    public function validateToken(SocialLoginData $data) {
        switch($data->provider) {
            case self::GOOGLE_PROVIDER:
                return $this->validateGoogleToken($data);
            case self::FACEBOOK_PROVIDER:
                return $this->validateFacebookToken($data);
            default:
                return false;
        }
    }

    /**
     * Validate google login data
     *
     * @param SocialLoginData $data
     * @return bool
     */
    public function validateGoogleToken(SocialLoginData $data) {
        $client = new Google_Client(['client_id' => $this->googleClientId]);
        $payload = $client->verifyIdToken($data->idToken);

        if (!$payload) {
            return false;
        }
        if ($payload['sub'] !== $data->id) {
            return false;
        }
        return true;
    }

    /**
     * validate facebook login data
     *
     * @param SocialLoginData $data
     * @return bool
     */
    public function validateFacebookToken(SocialLoginData $data) {
        try {
            $fb = new \Facebook\Facebook([
                'app_id' => $this->facebookAppid,
                'app_secret' => $this->facebookSecret,
            ]);

            $accessToken = $fb->getApp()->getAccessToken();
            $response = $fb->get('/debug_token?input_token=' . $data->token, $accessToken);
            $responseData = $response->getDecodedBody();
            $userId = $responseData['data']['user_id'];
            if ($userId !== $data->id) {
                return false;
            }
            return true;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            throw new ServiceUnavailableHttpException(10, 'Facebook service error');
        }
    }
}
