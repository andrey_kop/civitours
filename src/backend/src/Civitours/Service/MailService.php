<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.18
 * Time: 13:58
 */

namespace Civitours\Service;

use Pug\Facade as PugFacade;
use Pug\Pug;

/**
 * Service to send mails
 *
 * Class MailService
 * @package Civitours\Service
 */
class MailService
{

    const EMAIL_VERIFICATION_TEMPLATE_NAME = 'emailVerification';
    const PASSWORD_RESET_TEMPLATE_NAME = 'passwordReset';

    const MAIL_SENDER = 'no-reply@civitours.com';

    /**
     * Swift mailer
     *
     * @var \Swift_Mailer
     */
    private $mailer = null;

    /**
     *  Path to email templates
     *
     * @var string
     */
    private $pathToTemplates = null;

    /**
     * MailService constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param $pathToTemplates
     */
    public function __construct(\Swift_Mailer $mailer, $pathToTemplates)
    {
        $this->mailer = $mailer;
        $this->pathToTemplates = $pathToTemplates;
    }

    /**
     * Send verification mail to user
     *
     * @param $mailto
     * @param $link
     */
    public function sendVerificationEmail($mailto, $link)
    {
        $this->sendMail(
            $mailto,
            'Verify your mail',
            self::EMAIL_VERIFICATION_TEMPLATE_NAME,
            [
                'link' => $link,
                'mail' => $mailto
            ]
        );
    }

    /**
     * Send password reset mail to user
     *
     * @param $mailto
     * @param $link
     */
    public function sendPasswordResetEmail($mailto, $link)
    {
        $this->sendMail(
            $mailto,
            'Reset your password',
            self::PASSWORD_RESET_TEMPLATE_NAME,
            [
                'link' => $link,
                'mail' => $mailto
            ]
        );
    }

    /**
     * Send mail via mail transport
     *
     * @param $mailto
     * @param $subject
     * @param $templateName
     * @param array $params
     */
    protected function sendMail($mailto, $subject, $templateName, $params = [])
    {
        $message = \Swift_Message::newInstance(null)
            ->setSubject($subject)
            ->setFrom(self::MAIL_SENDER)
            ->setTo([$mailto])
            ->setBody(
                $this->parsePlainMessage(
                    $this->pathToTemplates . DIRECTORY_SEPARATOR. $templateName . '.plain', $params
                )
            )
            ->addPart(
                PugFacade::renderFile($this->pathToTemplates . DIRECTORY_SEPARATOR. $templateName . '.pug', $params),
                'text/html'
            );
        $this->mailer->send($message);
    }

    /**
     * Convert plain template to string with extended variables
     *
     * @param $template
     * @param array $params
     * @return bool|mixed|string
     */
    private function parsePlainMessage($template, $params = []) {
        $data = file_get_contents($template);
        foreach ($params as $name => $value) {
            $data = str_replace("%{$name}%", $value, $data);
        }
        return $data;
    }
}