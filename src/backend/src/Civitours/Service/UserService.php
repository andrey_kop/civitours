<?php

namespace Civitours\Service;

use Civitours\Entity\NewPasswordData;
use Civitours\Entity\RegistrationData;
use Civitours\Entity\SocialLoginData;
use Doctrine\DBAL\Connection;
use Rhumsaa\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;


/**
 * Service to perform operations with users
 *
 * Class UserService
 * @package Civitours\Service
 */
class UserService
{

    /**
     *  Database connection
     *
     * @var Connection
     */
    private $db = null;

    /**
     * Service to send mails
     *
     * @var MailService
     */
    private $mailService = null;

    /**
     * Link to confirm mail
     *
     * @var string
     */
    private $confirmLink = null;

    /**
     * Link to reset mail
     *
     * @var string
     */
    private $resetLink;

    /**
     * UserService constructor.
     *
     * @param Connection $db
     * @param MailService $mailService
     * @param $confirmLink
     * @param $resetLink
     */
    public function __construct(Connection $db, MailService $mailService, $confirmLink, $resetLink)
    {
        $this->db = $db;
        $this->mailService = $mailService;
        $this->confirmLink = $confirmLink;
        $this->resetLink = $resetLink;
    }

    /**
     * Register user in system
     *
     * @param RegistrationData $data
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function registerUser(RegistrationData $data) {
        $emailConfirmationCode = Uuid::uuid4();

        $this->db->beginTransaction();
        try {
            $queryBuilder = $this->db->createQueryBuilder();

            $queryBuilder
                ->insert('users')
                ->values([
                    'name'              => '?',
                    'surname'           => '?',
                    'email'             => '?',
                    'confirmation_code' => '?',
                    'password'          => '?',
                    'phone'             => '?',
                    'country'           => '?',
                    'city'              => '?'
                ])->setParameters([
                    $data->name,
                    $data->surname,
                    $data->email,
                    $emailConfirmationCode,
                    password_hash($data->password, PASSWORD_DEFAULT),
                    $data->phone,
                    $data->country,
                    $data->city
                ]);

            $queryBuilder->execute();
            $this->mailService->sendVerificationEmail($data->email, $this->buildConfirmationLink($emailConfirmationCode));
            $this->db->commit();
        } catch ( \Exception $e) {
            $this->db->rollBack();
            throw new ServiceUnavailableHttpException(1, $e->getMessage());
        }

    }

    /**
     * Resend confirmation email
     *
     * @param $email
     * @return bool
     */
    public function resendVerificationMail($email) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('confirmation_code')
            ->from('users')
            ->where( $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('email', '?'),
                $queryBuilder->expr()->isNotNull('confirmation_code')
            ))
            ->setMaxResults(1)
            ->setParameter(0, $email);

        $user = $queryBuilder->execute()->fetch();
        if (false === $user) {
            return false;
        }

        $this->mailService->sendVerificationEmail($email, $this->buildConfirmationLink($user['confirmation_code']));
        return true;
    }

    /**
     * Reset user password
     *
     * @param $email
     * @return bool
     */
    public function resetPassword($email) {
        $passwordReset = Uuid::uuid4();

        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('users')
            ->set('recovery_code', '?')
            ->where('email = ?')
            ->setParameter(0, $passwordReset)
            ->setParameter(1, $email);

        $queryBuilder->execute();
        $this->mailService->sendPasswordResetEmail($email, $this->buildResetLink($passwordReset));
        return true;

    }

    /**
     * Perform verify process
     *
     * @param $code
     * @return bool
     */
    public function verifyMail($code) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('id')
            ->from('users')
            ->where('confirmation_code = ?')
            ->setMaxResults(1)
            ->setParameter(0, $code);

        $user = $queryBuilder->execute()->fetch();
        if (false === $user) {
            return false;
        }

        $queryBuilder
            ->update('users')
            ->where('confirmation_code = ?')
            ->setParameter(0, $code)
            ->set('confirmation_code', 'NULL')
            ->set('email_confirmed', 'true');

        $queryBuilder->execute();
        return true;
    }

    /**
     * Find user by email
     *
     * @param $email
     * @return array|false
     */
    public function getUserByEmail($email) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('*')
            ->from('users')
            ->where('email = ?')
            ->setMaxResults(1)
            ->setParameter(0, $email);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Find user by social id
     *
     * @param $provider
     * @param $id
     * @return array|false
     */
    public function getUserBySocialId($provider, $id) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('*')
            ->from('users')
            ->where($provider . '_id = ?')
            ->setMaxResults(1)
            ->setParameter(0, $id);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Create new user from social data
     *
     * @param SocialLoginData $data
     */
    public function createUserFromSocialData(SocialLoginData $data) {

        list ($name, $surname) = explode(' ', $data->name);
        $queryBuilder = $this->db->createQueryBuilder();

        $user = $this->getUserByEmail($data->email);
        if ($user) {
            // link accounts
            $queryBuilder
                ->update('users')
                ->set($data->provider . '_id', '?')
                ->where('email = ?')
                ->setParameter(0, $data->id)
                ->setParameter(1, $data->email);
        } else {
            //create new account
            $queryBuilder
                ->insert('users')
                ->values([
                    'name'                  => '?',
                    'surname'               => '?',
                    'email'                 => '?',
                    'email_confirmed'       => '?',
                    'password'              => '?',
                    $data->provider . '_id' => '?'
                ])->setParameters([
                    $name,
                    $surname,
                    $data->email,
                    true,
                    'social',
                    $data->id
                ]);
        }

        $queryBuilder->execute();
    }

    /**
     * Update existing user from social data
     *
     * @param SocialLoginData $data
     * @return int
     */
    public function updateUserFromSocialData(SocialLoginData $data) {
        list ($name, $surname) = explode(' ', $data->name);

        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('users')
            ->set('name', '?')
            ->set('surname', '?')
            ->set('email', '?')
            ->where($data->provider . '_id = ?')
            ->setParameter(0, $name)
            ->setParameter(1, $surname)
            ->setParameter(2, $data->email)
            ->setParameter(3, $data->id);

        return $queryBuilder->execute();
    }

    /**
     * Find user by reset token
     *
     * @param $token
     * @return array|false
     */
    public function getUserByRecoveryToken($token) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('*')
            ->from('users')
            ->where('recovery_code = ?')
            ->setMaxResults(1)
            ->setParameter(0, $token);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Set new password
     *
     * @param NewPasswordData $data
     */
    public function setNewPassword(NewPasswordData $data) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('users')
            ->set('password', '?')
            ->set('recovery_code', 'NULL')
            ->where('recovery_code = ?')
            ->setParameter(0, password_hash($data->password, PASSWORD_DEFAULT))
            ->setParameter(1, $data->token);

        $queryBuilder->execute();
    }

    /**
     * Build confirmation link
     *
     * @param $emailConfirmationCode
     * @return string
     */
    private function buildConfirmationLink($emailConfirmationCode) {
        return $this->confirmLink . DIRECTORY_SEPARATOR . $emailConfirmationCode;
    }

    /**
     * Build reset link
     *
     * @param $resetConfirmationCode
     * @return string
     */
    private function buildResetLink($resetConfirmationCode) {
        return $this->resetLink . $resetConfirmationCode;
    }

    /**
     * Login user
     *
     * @param $email
     * @param $password
     * @return mixed
     */
    public function login($email, $password) {

        $user = $this->getUserByEmail($email);
        if (!$user) {
            return false;
        }

        if (!password_verify($password, $user['password'])) {
            return false;
        }
        return $user;

    }

}