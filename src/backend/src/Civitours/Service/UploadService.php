<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 31.05.18
 * Time: 15:55
 */

namespace Civitours\Service;


use BulletProof\Image;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

class UploadService
{
    private $tmpPath;
    private $galleryPath;
    private $imageUrl;
    private $dbPath;

    public function __construct($imagePath, $imageUrl)
    {
        $this->tmpPath = $imagePath . 'tmp' . DIRECTORY_SEPARATOR;
        $this->galleryPath = $imagePath . 'gallery' . DIRECTORY_SEPARATOR;
        $this->imageUrl = $imageUrl;
        $this->dbPath = 'gallery' . DIRECTORY_SEPARATOR;
    }

    /**
     * Upload image
     * @param $files
     * @param $params
     * @return array
     */
    public function uploadImage($files, $params) {
        $image = new Image($files);

        $image->setSize($params['min'], $params['max']);
        $image->setMime($params['mimes']);
        $image->setLocation($this->tmpPath);

        //generate filename
        $hash = uniqid();
        while(file_exists($this->tmpPath . $hash)) {
            $hash = uniqid();
        }

        $image->setName($hash);

        if($image['fileItem']){
            $upload = $image->upload();
            if($upload){
                return [
                    'imageName' => $image->getName() . '.' . $image->getMime(),
                    'imageUrl'  => $this->imageUrl . 'tmp/' . $image->getName() . '.' . $image->getMime()
                ];
            }else{
                throw new BadRequestHttpException($image['error']);
            }
        }
        throw new BadRequestHttpException('Wrong image is provided');
    }

    /**
     * Copy image for country and return db path
     *
     * @param $imageFile
     * @param $countryName
     * @return string
     */
    public function copyCountryImage($imageFile, $countryName) {
        $entityDir = $this->toFolderName($countryName);
        if ($res = $this->copyImage($imageFile, $entityDir)) {
            throw new ServiceUnavailableHttpException(10, $res);
        }
        return $this->dbPath . $entityDir . DIRECTORY_SEPARATOR . $imageFile;
    }

    /**
     * Copy image for city and return db path
     *
     * @param $imageFile
     * @param $countryName
     * @param $cityName
     * @return string
     */
    public function copyCityImage($imageFile, $countryName, $cityName) {
        $entityDir = $this->toFolderName($countryName) . DIRECTORY_SEPARATOR . $this->toFolderName($cityName);
        if ($res = $this->copyImage($imageFile, $entityDir)) {
            throw new ServiceUnavailableHttpException(10, $res);
        }
        return $this->dbPath . $entityDir . DIRECTORY_SEPARATOR . $imageFile;
    }

    /**
     * Copy image file for activity
     *
     * @param $imageFile
     * @param $countryName
     * @param $cityName
     * @param $idActivity
     * @return string
     */
    public function copyActivityImage($imageFile, $countryName, $cityName, $idActivity) {
        $entityDir = $this->toFolderName($countryName) . DIRECTORY_SEPARATOR . $this->toFolderName($cityName) . DIRECTORY_SEPARATOR . $idActivity;
        if ($res = $this->copyImage($imageFile, $entityDir)) {
            throw new ServiceUnavailableHttpException(10, $res);
        }
        return $this->dbPath . $entityDir . DIRECTORY_SEPARATOR . $imageFile;
    }

    /**
     * Copy image - filesystem operations
     *
     * @param $imageFile
     * @param $entityDir
     * @return string
     */
    private function copyImage($imageFile, $entityDir) {
        $tmpFile = $this->tmpPath . $imageFile;
        if(!file_exists($tmpFile)) {
            return 'Image not found: ' . $tmpFile;
        }
        $targetDir = $this->galleryPath . $entityDir;
        if(!file_exists($targetDir)) {
            mkdir($targetDir, 0777,true);
        }
        $result = copy($tmpFile, $targetDir . DIRECTORY_SEPARATOR . $imageFile);
        if (!$result) {
            return 'Cannot copy file: ' . $targetDir . DIRECTORY_SEPARATOR . $imageFile;
        }
        return '';
    }

    /**
     * Convert object name to folder name
     *
     * @param $string
     * @return string
     */
    private function toFolderName($string) {
        return strtolower(preg_replace('/\W/','', $string));
    }

}