<?php

namespace Civitours\Controller;

use Symfony\Component\Form\FormInterface;

/**
 * Trait to process forms
 *
 * Trait FormProcessTrait
 * @package Civitours\Controller
 */
trait FormProcessTrait {

    /**
     * Get form errors as array
     *
     * @param FormInterface $form
     * @return array
     */
    private function getErrorMessages(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors(true) as $error) {
            if ($error->getOrigin()) {
                $errors[$error->getOrigin()->getName()][] = $error->getMessage();
            }
        }
        return $errors;
    }
}