<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.05.18
 * Time: 11:32
 */

namespace Civitours\Controller;


use Civitours\Service\ReviewService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

class ReviewController
{
    /**
     * Retrieve reviews list for city
     *
     * @param Application $app
     * @param $idCity
     * @return JsonResponse
     */
    public function getForCity(Application $app, $idCity) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        return new JsonResponse($reviewService->getReviewsForCity($idCity));
    }

    /**
     * Retrieve reviews list for activity
     *
     * @param Application $app
     * @param $idActivity
     * @return JsonResponse
     */
    public function getForActivity(Application $app, $idActivity) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        return new JsonResponse($reviewService->getReviewsForActivity($idActivity));
    }
}