<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30.05.18
 * Time: 14:33
 */

namespace Civitours\Controller\Admin;


use Civitours\Service\ActivityService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Destination controller for admin
 *
 * Class DestinationController
 * @package Civitours\Controller\Admin
 */
class DestinationController
{

    /**
     * Get all available countries with activities
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function countryList(Application $app) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getCountriesList(false));
    }

    /**
     * Get all available countries with activities
     *
     * @param Application $app
     * @param $countryCode
     * @return JsonResponse
     */
    public function cityList(Application $app, $countryCode) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getCityList($countryCode, false));
    }
}
