<?php

namespace Civitours\Controller\Admin;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class Admin implements ControllerProviderInterface {

    public function connect(Application $app)
    {
        $admin = $app['controllers_factory'];

        $admin->get('/activities', "Civitours\Controller\Admin\ActivitiesController::getList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->post('/activities', "Civitours\Controller\Admin\ActivitiesController::create")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::getById")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->patch('/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::update")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->get('/destinations/country/list', "Civitours\Controller\Admin\DestinationController::countryList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/destinations/country/{countryCode}/cities', "Civitours\Controller\Admin\DestinationController::cityList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->post('/activities/{idActivity}/set/draft', "Civitours\Controller\Admin\ActivitiesController::setDraftStatus")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->post('/activities/{idActivity}/clear/draft', "Civitours\Controller\Admin\ActivitiesController::clearDraftStatus")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->patch('/countries/{id}', "Civitours\Controller\Admin\CountryController::update")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('id', '\d+');

        $admin->patch('/cities/{id}', "Civitours\Controller\Admin\CityController::update")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('id', '\d+');

        return $admin;
    }

}