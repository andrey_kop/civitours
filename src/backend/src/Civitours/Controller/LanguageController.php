<?php

namespace Civitours\Controller;

use Civitours\Service\LanguageService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class to operate languages
 *
 * Class LanguageController
 * @package Civitours\Controller
 */
class LanguageController {

    /**
     * @param Application $app
     * @return JsonResponse
     */
    public function getList(Application $app) {
        /** @var LanguageService $languageService */
        $languageService = $app['language.service'];
        return new JsonResponse($languageService->getLanguagesList());
    }

}
