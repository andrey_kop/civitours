<?php

namespace Civitours\Controller;

use Civitours\Entity\SocialLoginData;
use Civitours\Form\NewPasswordFormType;
use Civitours\Form\SocialLoginType;
use Civitours\Service\UserService;
use Rhumsaa\Uuid\Uuid;
use Silex\Application;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthenticateController
{

    use FormProcessTrait;

    /**
     * login user
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticate(Application $app, Request $request)
    {

        $rawHeader = $request->headers->get('Authorization');
        if (strpos($rawHeader, 'Basic ') === false) {
            throw new BadRequestHttpException('Bad credentials are provided');
        }
        $token = str_replace('Basic ', '', $rawHeader);
        $decodedToken = base64_decode($token);
        list ($username, $password) = explode(':', $decodedToken);

        if (!$username || !$password) {
            throw new BadRequestHttpException('Bad credentials are provided');
        }

        /** @var UserService $userService */
        $userService = $app['user.service'];
        $user = $userService->login($username, $password);

        if (!$user) {
            throw new AccessDeniedHttpException('Failed to Authenticate');
        }

        if (!$user['email_confirmed']) {
            throw new AccessDeniedHttpException('Please confirm your email');
        }

        return new JsonResponse(['token' => $this->generateToken($app, $user)]);
    }

    /**
     * login user by social
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticateSocial(Application $app, Request $request)
    {
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(SocialLoginType::class, null, [
            'social_service'      => $app['social.service']
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var SocialLoginData $data */
            $data = $form->getViewData();
            /** @var UserService $userService */
            $userService = $app['user.service'];
            $user = $userService->getUserBySocialId($data->provider, $data->id);
            if (false === $user) {
                $userService->createUserFromSocialData($data);
                $user = $userService->getUserBySocialId($data->provider, $data->id);
            } else {
                $userService->updateUserFromSocialData($data);
            }
            return new JsonResponse(['token' => $this->generateToken($app, $user)]);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Generate token for user
     *
     * @param Application $app
     * @param $user
     * @return string
     */
    private function generateToken(Application $app, $user) {
        $tokenId    = Uuid::uuid4();
        $issuedAt   = time();
        $notBefore  = $issuedAt;
        $expire     = $notBefore + 604800; // Adding 1 week
        $serverName = $app['server.name'];

        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'iss'  => $serverName,       // Issuer
            'nbf'  => $notBefore,        // Not before
            'exp'  => $expire,           // Expire
            'data' => [                  // Data related to the signer user
                'userId'   => $user['id'],      // userid from the users table
                'userName' => $user['name'],    // User name
                'isAdmin'  => $user['is_admin'] // Has admin role
            ]
        ];
        // Get the secret key for signing the JWT from an environment variable
        $secretKey = base64_decode($app['secret']);

        $algorithm = $app['algorithm'];
        // Sign the JWT with the secret key
        return JWT::encode(
            $data,
            $secretKey,
            $algorithm
        );
    }

    /**
     * Request password reset
     *
     * @param Application $app
     * @param Request $request
     * @return Response
     */
    public function resetRequest(Application $app, Request $request) {
        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData || empty($submittedData['email'])) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        /** @var UserService $userService */
        $userService = $app['user.service'];
        if (!$userService->getUserByEmail($submittedData['email'])) {
            throw new NotFoundHttpException('User not found');
        }
        $userService->resetPassword($submittedData['email']);

        return new JsonResponse([
            'message' => 'Email successfully sent. Follow instructions to reset password'
        ]);
    }

    /**
     * Set new password
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function newPassword(Application $app, Request $request)
    {
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(NewPasswordFormType::class, null, [
            'user_service'      => $app['user.service']
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var UserService $userService */
            $userService = $app['user.service'];
            $userService->setNewPassword($form->getViewData());
            return new Response(null, Response::HTTP_NO_CONTENT);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

}
