<?php

namespace Civitours\Controller;

use Civitours\Service\GeoDataService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller to operate with countries
 *
 * Class CountryController
 * @package Civitours\Controller
 */
class CountryController
{
    /**
     * retrieve all countries information
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getAll(Application $app) {
        /** @var GeoDataService $geoDataService */
        $geoDataService = $app['geo_data.service'];
        return new JsonResponse($geoDataService->getCountries());
    }

    /**
     * Retrieve one country information by id
     *
     * @param Application $app
     * @param $id
     * @return JsonResponse
     */
    public function get(Application $app, $id) {
        /** @var GeoDataService $geoDataService */
        $geoDataService = $app['geo_data.service'];
        $country = $geoDataService->getCountry($id);
        if (false === $country) {
            throw new NotFoundHttpException("Country not found");
        }
        return new JsonResponse($country);
    }
}