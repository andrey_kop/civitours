<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30.05.18
 * Time: 14:21
 */

namespace Civitours\Controller;


class AdminController
{
    /**
     * Get all available countries with activities
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function countryList(Application $app) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getCountriesList());
    }
}