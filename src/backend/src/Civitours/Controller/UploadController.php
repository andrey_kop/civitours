<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.05.18
 * Time: 15:06
 */

namespace Civitours\Controller;


use BulletProof\Image;
use Civitours\Service\UploadService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Controller to upload files
 *
 * Class UploadController
 * @package Civitours\Controller
 */
class UploadController
{

    /**
     * Check and upload image
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function uploadImage(Application $app) {
        /** @var UploadService $uploadService */
        $uploadService = $app['upload.service'];
        return new JsonResponse($uploadService->uploadImage($_FILES, $app['upload.parameters']));
    }
}