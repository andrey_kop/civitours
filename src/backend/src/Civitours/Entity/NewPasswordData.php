<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.05.18
 * Time: 18:06
 */

namespace Civitours\Entity;

/**
 * Class NewPasswordData
 * @package Civitours\Entity
 */
class NewPasswordData
{
    public $password;
    public $passwordConfirm;
    public $token;
}
