<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.05.18
 * Time: 13:10
 */

namespace Civitours\Entity;


class SocialLoginData
{
    public $id;
    public $name;
    public $image;
    public $email;
    public $provider;
    public $token;
    public $idToken;
}
