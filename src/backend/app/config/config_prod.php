<?php

// Prod CONFIGURATION
$app['environment'] = "prod";
$app['debug'] = false;
$app['log.level'] = Monolog\Logger::ERROR;
$app['server.name'] = 'civitours.com';
$app['api'] = '/api/v1';

// PARAMETERS
$app['driver'] = 'pdo_pgsql';
$app['host'] = 'localhost';
$app['port'] = '5432';
$app['dbname'] = 'civitours';
$app['user'] = 'civitours';
$app['password'] = 'RbgGLCZrNZP6RkuV';

// Mailer
$app['mailer.options'] = array(
    'host' => 'civitours.com',
    'port' => '465',
    'username' => 'support@civitours.com',
    'password' => '1qaz@WSX',
    'encryption' => 'ssl',
    'auth_mode' => null
);

$app['base.url'] = 'https://civitours.com';
$app['api.url'] = $app['base.url'] . '/api/v1';
$app['image.url'] = 'https://civitours.com/api/img/';
$app['image.path'] = __DIR__ . '/../../public/img/';