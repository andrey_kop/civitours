<?php


//TODO: fill with correct settings

// Key for signing the JWT's, I suggest generate it with base64_encode(openssl_random_pseudo_bytes(64))
$app['secret'] =  base64_encode("secret");
$app['algorithm'] = "HS512";

$app['mailer_service.template_path'] = __DIR__ . '/../resources/templates/mails';

//Recaptcha settings
$app['recapthca.secret'] = '6Ldc9lUUAAAAAF87KoukOOQnfu-qC9Pr5XHvz5vO';

//Social settings
$app['social.google.clientId'] = '785247191567-vrngdd82na04r0sap0vcfqj94jj1jjoa.apps.googleusercontent.com';
$app['social.facebook.appid'] = '1626669250713987';
$app['social.facebook.secret'] = '394ca28d6b48846ba82fd20ba9b44dc8';

$app['upload.parameters'] = [
    'min'   => 1024,                            // Image min size 1Kb
    'max'   => 5242880,                         // Image max size 5Mb
    'mimes' => ['jpg', 'png', 'gif', 'jpeg']    // Image allowable mime types
];
