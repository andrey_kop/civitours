<?php

$app->mount($app['api'].'/books', new Civitours\Controller\Provider\Book());

// Admin endpoints
$app->mount($app['api'].'/admin', new Civitours\Controller\Admin\Admin());

// User auth endpoints
$app->post($app['api'].'/register', "Civitours\Controller\RegistrationController::register");
$app->post($app['api'].'/resend', "Civitours\Controller\RegistrationController::resend");
$app->get($app['api'].'/confirm/{token}', "Civitours\Controller\RegistrationController::confirm");
$app->get($app['api'].'/login', "Civitours\Controller\AuthenticateController::authenticate");
$app->post($app['api'].'/login/social', "Civitours\Controller\AuthenticateController::authenticateSocial");

$app->match($app['api'].'/restore', "Civitours\Controller\AuthenticateController::restore");
$app->match($app['api'].'/reset_request', "Civitours\Controller\AuthenticateController::resetRequest");
$app->match($app['api'].'/new_password', "Civitours\Controller\AuthenticateController::newPassword");

// Geo data routes
$app->get($app['api'].'/countries', "Civitours\Controller\CountryController::getAll");
$app->get($app['api'].'/countries/{id}', "Civitours\Controller\CountryController::get")
    ->assert('id', '\d+');
$app->get($app['api'].'/cities/{id}', "Civitours\Controller\CityController::get")
    ->assert('id', '\d+');
$app->get($app['api'].'/cities/{country}/search/{name}', "Civitours\Controller\CityController::search")
    ->assert('country', '\d+');

//Destination routes
$app->get($app['api'].'/destinations/country/list', "Civitours\Controller\DestinationController::countryList");
$app->get($app['api'].'/destinations/main/list', "Civitours\Controller\DestinationController::mainList");

$app->get($app['api'].'/destinations/country/{countryCode}', "Civitours\Controller\DestinationController::getDestinations");
$app->get($app['api'].'/destinations/country/{countryCode}/cities', "Civitours\Controller\DestinationController::cityList");
$app->get($app['api'].'/destinations/city/{cityCode}', "Civitours\Controller\DestinationController::getDestinationCity");

//Activities routes
$app->get($app['api'].'/activities/featured', "Civitours\Controller\ActivitiesController::featured");
$app->get($app['api'].'/activities/{idActivity}', "Civitours\Controller\ActivitiesController::getById")
    ->assert('idActivity', '\d+');
$app->get($app['api'].'/activities/{idActivity}/related', "Civitours\Controller\ActivitiesController::getRelated")
    ->assert('idActivity', '\d+');
$app->get($app['api'].'/activities/categories', "Civitours\Controller\ActivitiesController::getCategories");
$app->get($app['api'].'/activities/city/{idCity}', "Civitours\Controller\ActivitiesController::getForCity")
    ->assert('idCity', '\d+');

//Review routes
$app->get($app['api'].'/reviews/city/{idCity}', "Civitours\Controller\ReviewController::getForCity")
    ->assert('idCity', '\d+');
$app->get($app['api'].'/reviews/activity/{idActivity}', "Civitours\Controller\ReviewController::getForActivity")
    ->assert('idActivity', '\d+');

// Upload routes
$app->post($app['api'].'/upload/image', "Civitours\Controller\UploadController::uploadImage");

// Language routes
$app->get($app['api'].'/languages',  "Civitours\Controller\LanguageController::getList");