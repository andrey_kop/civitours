<?php

$app->mount('{wildcard}' . $app['api'].'/books', new Civitours\Controller\Provider\Book());

// Admin endpoints
$app->get('{wildcard}' . $app['api'].'/admin/activities', "Civitours\Controller\Admin\ActivitiesController::getList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/activities', "Civitours\Controller\Admin\ActivitiesController::create")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::getById")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->patch('{wildcard}' . $app['api'].'/admin/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/destinations/country/list', "Civitours\Controller\Admin\DestinationController::countryList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/activities/{idActivity}/set/draft', "Civitours\Controller\Admin\ActivitiesController::setDraftStatus")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/activities/{idActivity}/clear/draft', "Civitours\Controller\Admin\ActivitiesController::clearDraftStatus")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/destinations/country/{countryCode}/cities', "Civitours\Controller\Admin\DestinationController::cityList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->patch('{wildcard}' . $app['api'].'/admin/countries/{id}', "Civitours\Controller\Admin\CountryController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->patch('{wildcard}' . $app['api'].'/admin/cities/{id}', "Civitours\Controller\Admin\CityController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');

// User auth endpoints
$app->post('{wildcard}' . $app['api'].'/register', "Civitours\Controller\RegistrationController::register")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/resend', "Civitours\Controller\RegistrationController::resend")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/confirm/{token}', "Civitours\Controller\RegistrationController::confirm")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/login', "Civitours\Controller\AuthenticateController::authenticate")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/login/social', "Civitours\Controller\AuthenticateController::authenticateSocial")
    ->assert('wildcard', '.*');

$app->match('{wildcard}' . $app['api'].'/restore', "Civitours\Controller\AuthenticateController::restore")
    ->assert('wildcard', '.*');
$app->match('{wildcard}' . $app['api'].'/reset_request', "Civitours\Controller\AuthenticateController::resetRequest")
    ->assert('wildcard', '.*');
$app->match('{wildcard}' . $app['api'].'/new_password', "Civitours\Controller\AuthenticateController::newPassword")
    ->assert('wildcard', '.*');

// Geo data routes
$app->get('{wildcard}' . $app['api'].'/countries', "Civitours\Controller\CountryController::getAll")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/countries/{id}', "Civitours\Controller\CountryController::get")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/cities/{id}', "Civitours\Controller\CityController::get")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/cities/{country}/search/{name}', "Civitours\Controller\CityController::search")
    ->assert('country', '\d+')
    ->assert('wildcard', '.*');

//Destination routes
$app->get('{wildcard}' . $app['api'].'/destinations/country/list', "Civitours\Controller\DestinationController::countryList")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/destinations/main/list', "Civitours\Controller\DestinationController::mainList")
    ->assert('wildcard', '.*');

$app->get('{wildcard}' . $app['api'].'/destinations/country/{countryCode}', "Civitours\Controller\DestinationController::getDestinations")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/destinations/country/{countryCode}/cities', "Civitours\Controller\DestinationController::cityList")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/destinations/city/{cityCode}', "Civitours\Controller\DestinationController::getDestinationCity")
    ->assert('wildcard', '.*');


//Activities routes
$app->get('{wildcard}' . $app['api'].'/activities/featured', "Civitours\Controller\ActivitiesController::featured")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/{idActivity}', "Civitours\Controller\ActivitiesController::getById")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/{idActivity}/related', "Civitours\Controller\ActivitiesController::getRelated")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/categories', "Civitours\Controller\ActivitiesController::getCategories")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/city/{idCity}', "Civitours\Controller\ActivitiesController::getForCity")
    ->assert('idCity', '\d+')
    ->assert('wildcard', '.*');

//Review routes
$app->get('{wildcard}' . $app['api'].'/reviews/city/{idCity}', "Civitours\Controller\ReviewController::getForCity")
    ->assert('idCity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/reviews/activity/{idActivity}', "Civitours\Controller\ReviewController::getForActivity")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');

// Upload routes
$app->post('{wildcard}' . $app['api'].'/upload/image', "Civitours\Controller\UploadController::uploadImage")
    ->assert('wildcard', '.*');

// Language routes
$app->get('{wildcard}' . $app['api'].'/languages',  "Civitours\Controller\LanguageController::getList")
    ->assert('wildcard', '.*');