import { HomePage } from './app.po';
import { browser, protractor } from 'protractor';

describe('Booking calendar test', () => {
  let page: HomePage;

  beforeEach(() => {
    page = new HomePage();
  });


  it('should have all components after loading', () => {
    page.navigateTo();
    expect(page.getDateBlock().isPresent()).toBe(true);
    expect(page.getTimeBlock().isPresent()).toBe(true);
    expect(page.getTicketBlock().isPresent()).toBe(true);
    expect(page.getBookButton().isPresent()).toBe(true);

  });

  it('should open types select and select a value', () => {
    page.navigateTo();

    let typesHeader = page.getTypesPanelHeader();
    typesHeader.click();

    expect(page.getTypesPanel().isPresent()).toBe(true);
    let type = page.getLastTypeElement();
    let typeString = type.getText();
    type.click();

    expect(page.getTypesPanel().isPresent()).toBe(false);
    expect(page.getTypesPanelHeaderText()).toEqual(typeString);

  });

  it('should have one available date an select it', () => {
    page.navigateTo();

    let availableElement = page.getAvailableDateElement();
    let selectedElement = page.getSelectedDateElement();
    expect(availableElement.isPresent()).toBe(true);
    expect(selectedElement.isPresent()).toBe(false);

    availableElement.click();
    expect(selectedElement.isPresent()).toBe(true);
    expect(selectedElement.getText()).toEqual(availableElement.getText());
    expect(page.getFirstTimeSelector().isPresent()).toBe(true);

  });

  it('should add ticket name to header', async () => {
    page.navigateTo();

    expect(page.getTicketsPanelHeaderText()).toEqual('People');
    page.getTicketsPanelHeader().click();
    let firstTicketName = await page.getFirstTicketHeaderText();
    page.getFirstTicketIncrease().click();
    expect(page.getTicketsPanelHeaderText()).toEqual('1 ' + firstTicketName);
    page.getFirstTicketDecrease().click();
    expect(page.getTicketsPanelHeaderText()).toEqual('People');
  });

  it('should check form before submit', () => {
      page.navigateTo();
      browser.executeScript('window.onbeforeunload = ()=>{}');

      let submit = page.getBookButton();
      let tooltips = page.getTooltips();
      submit.click();
      expect(tooltips.count()).toBeGreaterThan(0);

      page.getAvailableDateElement().click();
      page.getFirstTimeSelector().click();
      page.getTicketsPanelHeader().click();
      page.getFirstTicketIncrease().click();
      submit.click();
      expect(tooltips.count()).toEqual(0);
  });

});
