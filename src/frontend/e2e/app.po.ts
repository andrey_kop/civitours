import { browser, by, element } from 'protractor';

export class HomePage {

  navigateTo() {
    return browser.get('/home');
  }

  getCalendarElement() {
    return element(by.css('app-booking-calendar'));
  }

  getAvailableDateElement() {
    return this.getCalendarElement().all(by.css('.btn-light:not(.text-muted)')).first();
  }

  getSelectedDateElement() {
    return this.getCalendarElement().element(by.css('.btn-light.bg-primary'));
  }

  getFirstTimeSelector() {
    return this.getCalendarElement().all(by.css('.timeselect')).first();
  }

  getBookButton() {
    return this.getCalendarElement().element(by.css('button.order-button'));
  }

  getDateBlock() {
    return this.getCalendarElement().element(by.css('ngb-datepicker'));
  }

  getTimeBlock() {
    return this.getCalendarElement().element(by.css('.time-labels'));
  }

  getTicketBlock() {
    return this.getCalendarElement().element(by.css('.people-div'));
  }

  getTypesPanelHeader() {
    return this.getCalendarElement().element(by.css('#types-panel-header a'));
  }

  getTypesPanelHeaderText() {
    return this.getCalendarElement().element(by.css('#types-panel-header a span')).getText();
  }

  getTypesPanel() {
    return this.getCalendarElement().element(by.css('#types-panel'));
  }

  getLastTypeElement() {
    return this.getCalendarElement().element(by.css('#types-panel .types:last-child'));
  }

  getTicketsPanelHeader() {
    return this.getCalendarElement().element(by.css('.people-div a'));
  }

  getTicketsPanelHeaderText() {
    return this.getCalendarElement().element(by.css('.people-div a span')).getText();
  }

  getFirstTicketIncrease() {
    return this.getCalendarElement().all(by.css('.people-div .tickets'))
      .first().all(by.css('button')).last();
  }

  getFirstTicketDecrease() {
    return this.getCalendarElement().all(by.css('.people-div .tickets'))
      .first().all(by.css('button')).first();
  }

  getFirstTicketHeaderText() {
    return this.getCalendarElement().all(by.css('.people-div .ticket-header')).first().getText();
  }

  getTooltips() {
    return this.getCalendarElement().all(by.css('ngb-tooltip-window'));
  }
}
