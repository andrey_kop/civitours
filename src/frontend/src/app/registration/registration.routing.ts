import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration.component';

const routes: Routes = [
  {
    path: '', component: RegistrationComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Sign up'
      }
    },
  },
];

export const RegistrationRoutes = RouterModule.forChild(routes);
