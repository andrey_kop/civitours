import { NgModule } from '@angular/core';
import { TransferState } from '@angular/platform-browser';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { TranslatesService } from '@shared/translates/translates.service';

import { TranslatesServerLoaderService } from './translates-server-loader.service';
import { CustomDatepickerI18n } from '@shared/translates/custom.datepicker.i18n';
import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';

export function translateFactory(transferState: TransferState): TranslatesServerLoaderService {
  return new TranslatesServerLoaderService('./dist/assets/i18n', '.json', transferState);
}

@NgModule({
  imports: [
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateFactory,
        deps: [TransferState]
      }
    }),
  ],
  providers: [
    TranslatesService,
    {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}
  ]
})
export class TranslatesServerModule {
}
