interface UploadResult {
  imageName: string;
  imageUrl:  string;
}
