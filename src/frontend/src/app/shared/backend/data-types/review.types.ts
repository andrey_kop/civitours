export interface Review {
  id:         number;
  header:     string;
  text:       string;
  rate:       number;
  author:     string;
  activity:   number;
  created_at: string;
}
