export class RegistrationForm {

  constructor(
    public name = '',
    public surname = '',
    public email = '',
    public password = '',
    public passwordConfirm = '',
    public country = 0,
    public city = 0,
    public phone = '',
    public token = '') {}

  public fillFromObject(obj: any) {
    this.name             = obj.name || this.name;
    this.surname          = obj.surname || this.surname;
    this.email            = obj.email || this.email;
    this.password         = obj.password || this.password;
    this.passwordConfirm  = obj.passwordConfirm || this.passwordConfirm;
    this.country          = obj.country || this.country;
    this.city             = obj.city || this.city;
    this.phone            = obj.phone || this.phone;
    this.token            = obj.token || this.token;
  }

}
