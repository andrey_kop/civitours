export interface Country {
  id:             number;
  code:           string;
  name:           string;
  currency:       string;
  calling_code:   string;
  languages:      string;
  avatar:         string;
  locale_name:    LocaleNames;
}

export interface City {
  id:           number;
  country:      number;
  name:         string;
  avatar:       string;
  offset:       number;
  locale_name:  LocaleNames;
}

export interface Language {
  code:         string;
  name:         string;
  native_name:  string;
}

export interface LocaleNames {
  [locale: string]: string
}
