import { Injectable } from '@angular/core';
import { DestinationCity, DestinationCountry } from '@shared/backend/data-types/destination.types';
import {
  Activity,
  ActivityFull,
  Category
} from '@shared/backend/data-types/activity.types';
import { City, Country, Language } from '@shared/backend/data-types/geo.types';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { RegistrationForm } from '@shared/backend/data-types/registration-form';
import { AppConfig } from '@shared/config/app-config.service';
import { Review } from '@shared/backend/data-types/review.types';
import { AjaxActivityFilter} from '@shared/components/data-types/filter.types';
import { CountrySaveForm } from '../../admin/data-types/country-save-form';
import { CitySaveForm } from '../../admin/data-types/city-save-form';
import { ActivityForm } from '../../admin/data-types/activity-form';


@Injectable()
export class RestApiService {

  private readonly apiUrl = null;
  private categoriesCache: Promise<Category[]>;

  constructor(private http: HttpClient) {
    this.apiUrl = AppConfig.getConfiguration().backendUrl;
  }

  /**
   * Api request for retrieving destination data
   *
   * @returns {Promise<DestinationCity[]>}
   */
  public getMainDestinationsList(): Promise<DestinationCity[]> {
    return this.http.get<DestinationCity[]>(this.apiUrl + 'destinations/main/list').toPromise();
  }

  /**
   * Retrieve full list of activities
   *
   * @param forAdmin
   */
  public getActivities(forAdmin = false): Promise<Activity[]> {
    const adminAdd = forAdmin ? 'admin/' : '';
    return this.http.get<Activity[]>(this.apiUrl + adminAdd + 'activities').toPromise();
  }

  /**
   * Create new activity
   *
   * @param {ActivityForm} form
   * @returns {Promise<Object>}
   */
  public createActivity(form: ActivityForm) {
    return this.http.post(this.apiUrl + 'admin/activities', form).toPromise();
  }

  /**
   * Edit existent activity
   * @param {number} id
   * @param {ActivityForm} form
   * @returns {Promise<Object>}
   */
  public updateActivity(id: number, form: ActivityForm) {
    return this.http.patch(this.apiUrl + 'admin/activities/' + id, form).toPromise();
  }

  /**
   * Set activity draft status
   *
   * @param {number} idActivity
   * @param {boolean} draftStatus
   * @returns {Promise<Object>}
   */
  public setActivityDraftStatus(idActivity: number, draftStatus: boolean) {
    const actionString = draftStatus ? 'set' : 'clear';
    return this.http.post(this.apiUrl + `admin/activities/${idActivity}/${actionString}/draft`, {}).toPromise();
  }

  /**
   * Retrieve list of featured activities
   *
   * @param {number} limit
   * @param {number} offset
   * @param country
   */
  public getFeaturedActivities(limit = 6, offset = 0, country: string = null): Promise<Activity[]> {
    const queryString = `?s=${offset}&l=${limit}` + (country ? `&c=${country}` : '' );
    return this.http.get<Activity[]>(this.apiUrl + `activities/featured${queryString}`).toPromise();
  }

  /**
   * Retrieve all available categories for activities
   *
   * @returns {Promise<Category>}
   * @constructor
   */
  public getActivityCategoriesList(): Promise<Category[]> {
    if(!this.categoriesCache) {
      this.categoriesCache = this.http.get<Category[]>(this.apiUrl + 'activities/categories').toPromise();
    }
    return this.categoriesCache;
  }

  /**
   * Retrieve all activities for city in short format
   *
   * @param {number} idCity
   * @param filter
   * @returns {Promise<Activity[]>}
   */
  public getActivitiesForCity(idCity: number, filter?: AjaxActivityFilter): Promise<Activity[]> {
    let filterString = '';
    if (filter) {
      filterString = '?';
      filter.dates.forEach(interval => filterString += `d[]=${interval.start}:${interval.end}&`);
      filter.times.forEach(interval => filterString += `t[]=${interval.start}:${interval.end}&`);
    }
    return this.http.get<Activity[]>(this.apiUrl + `activities/city/${idCity}${filterString}`).toPromise();
  }

  /**
   * Retrieve list of related activities
   *
   * @param {number} id
   * @returns {Promise<Activity[]>}
   */
  public getRelatedActivities(id: number): Promise<Activity[]> {
    return this.http.get<Activity[]>(this.apiUrl + `activities/${id}/related`).toPromise();
  }

  /**
   * Get full destination info for country
   * @param {string} countryCode
   * @returns {Promise<DestinationCountry>}
   */
  public getDestinationCountry(countryCode: string): Promise<DestinationCountry> {
    return this.http.get<DestinationCountry>(this.apiUrl + 'destinations/country/' + countryCode).toPromise();
  }

  /**
   * Get full destination info for city
   * @param {string} cityCode
   * @returns {Promise<DestinationCity>}
   */
  public getDestinationCity(cityCode: string): Promise<DestinationCity> {
    return this.http.get<DestinationCity>(this.apiUrl + 'destinations/city/' + cityCode).toPromise();
  }

  /**
   * Get list of available destinations for country
   * @returns {Promise<Country[]>}
   */
  public getDestinationCountryList(forAdmin = false): Promise<Country[]> {
    const adminAdd = forAdmin ? 'admin/' : '';
    return this.http.get<Country[]>(this.apiUrl + adminAdd + 'destinations/country/list').toPromise();
  }

  /**
   * Get destination cities for country
   *
   * @param countryCode
   * @param forAdmin
   * @returns {Promise<City[]>}
   */
  public getDestinationCityList(countryCode: any, forAdmin = false): Promise<City[]> {
    const adminAdd = forAdmin ? 'admin/' : '';
    return this.http.get<City[]>(this.apiUrl + `${adminAdd}destinations/country/${countryCode}/cities`).toPromise();
  }

  /**
   * Retrieve full information about activity including
   *
   * @param {number} id
   * @param forAdmin
   * @returns {Promise<ActivityFull>}
   */
  public getActivityFull(id: number, forAdmin = false): Promise<ActivityFull> {
    const adminAdd = forAdmin ? 'admin/' : '';
    return this.http.get<ActivityFull>(this.apiUrl + adminAdd  + 'activities/' + id).toPromise();
  }

  /**
   * Retrieve all countries list
   * @returns {Promise<Country[]>}
   */
  public getCountryList(): Promise<Country[]> {
    return this.http.get<Country[]>(this.apiUrl + 'countries').toPromise();
  }

  /**
   * Retrieve cities list by country and search string
   *
   * @param {number} country
   * @param {string} search
   * @returns {Promise<City[]>}
   */
  public searchCities(country: number, search: string): Promise<City[]> {
    return this.http.get<City[]>(this.apiUrl + 'cities/' + country + '/search/' + search).toPromise();
  }

  /**
   * Make user registration
   *
   * @param {RegistrationForm} form
   * @returns {Promise<any>}
   */
  public registerUser(form: RegistrationForm): Promise<any> {
    return this.http.post(this.apiUrl + 'register', form).toPromise();
  }

  /**
   * Retrieve review list for city
   * @param {number} idCity
   * @returns {Promise<Review[]>}
   */
  public getReviewsForCity(idCity: number): Promise<Review[]> {
    return this.http.get<Review[]>(this.apiUrl + `reviews/city/${idCity}`).toPromise();
  }

  /**
   * Retrieve review list for activity
   * @param {number} idActivity
   * @returns {Promise<Review[]>}
   */
  public getReviewsForActivity(idActivity: number): Promise<Review[]> {
    return this.http.get<Review[]>(this.apiUrl + `reviews/activity/${idActivity}`).toPromise();
  }

  /**
   * Retrieve list of languages
   * @returns {Promise<Language[]>}
   */
  public getLanguages(): Promise<Language[]> {
    return this.http.get<Language[]>(this.apiUrl + 'languages').toPromise();
  }

  /**
   * Update country in database
   * @returns {Promise<any>}
   */
  public saveCountryData(idCountry: number, form: CountrySaveForm): Promise<any> {
    return this.http.patch(this.apiUrl + `admin/countries/${idCountry}`, form).toPromise();
  }

  /**
   * Update country in database
   * @returns {Promise<any>}
   */
  public saveCityData(idCity: number, form: CitySaveForm): Promise<any> {
    return this.http.patch(this.apiUrl + `admin/cities/${idCity}`, form).toPromise();
  }

  /**
   * Endpoint to upload image
   *
   * @param {File} fileItem
   * @returns {any}
   */
  public uploadImage (fileItem:File):any {
    const formData: FormData = new FormData();

    formData.append('fileItem', fileItem, fileItem.name);
    const req = new HttpRequest('POST', this.apiUrl + 'upload/image', formData, {
      reportProgress: true // for progress data
    });
    return this.http.request(req)
  }

}
