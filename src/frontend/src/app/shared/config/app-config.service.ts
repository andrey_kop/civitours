import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { IAppConfig } from '@shared/config/DataTypes/i-app-config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppConfig {

  private static config: IAppConfig = null;
  constructor(private http:HttpClient) {}

  load() {
    let envString = environment.production ? 'prod' : 'dev';
    let configFile = `assets/config/config.${envString}.json`;
    return this.http.get<IAppConfig>(configFile).toPromise()
      .then(config => AppConfig.config = config)
  }

  static getConfiguration():IAppConfig {
    return AppConfig.config;
  }
}