import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(public auth: AuthService, public router: Router) {}

  /**
   * Guard function
   * @returns {boolean}
   */
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate([AuthService.FALLBACK_URL], {queryParams: {modal: 'login'}});
      return false;
    }
    return true;
  }

}

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}

  /**
   * Guard function
   * @returns {boolean}
   */
  canActivate(): boolean {
    if(!this.auth.isAdmin()) {
      this.router.navigate([AuthService.UNAUTHORIZED_URL]);
      return false;
    }
    return true;
  }
}
