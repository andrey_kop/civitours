import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '@shared/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.styl']
})
export class NewPasswordComponent implements OnInit {

  serverErrors = {
    password: [],
    passwordConfirm: [],
    token: []
  };

  newPasswordForm: FormGroup;

  checkFields = false;
  inProgress = false;

  passwordType = "password";
  passwordConfirmType = "password";

  newPasswordError = '';

  constructor(public activeModal: NgbActiveModal, private auth: AuthService, private router: Router) {
    this.createForm();
  }

  /**
   * Create form class
   */
  private createForm() {

    this.newPasswordForm = new FormGroup({
      password:   new FormControl('',   [
        Validators.required,
        Validators.pattern(/^(?=.*[A-Z]).{8,}$/)
      ]),
      token:  new FormControl('',   [
        Validators.required
      ])
    }, { updateOn: 'blur'});

    const passwordConfirm = new FormControl('',   [
      Validators.required,
      this.sameValueAs(this.newPasswordForm, 'password')
    ]);

    this.newPasswordForm.addControl('passwordConfirm', passwordConfirm);
    for(let control in this.newPasswordForm.controls) {
      this.newPasswordForm.controls[control].valueChanges.subscribe(() => {
        this.serverErrors[control] = []; // Reset server error for this control
      })
    }
  }

  /**
   * Custom validation function
   *
   * @param {FormGroup} group
   * @param {string} controlName
   * @returns {ValidatorFn}
   */
  sameValueAs(group: FormGroup, controlName: string): ValidatorFn {
    return (control: AbstractControl) => {
      const myValue = control.value;
      const compareValue = group.controls[controlName].value;
      return (myValue === compareValue) ? null : {valueDifferent: true};
    };
  }

  /**
   * Token setter
   *
   * @param {string} code
   */
  @Input() set code(code: string) {
    this.newPasswordForm.get('token').setValue(code);
  }

  ngOnInit() {
  }

  onSubmit() {
    this.checkFields = true;
    if (this.newPasswordForm.invalid) {
      return;
    }
    this.inProgress = true;
    this.auth.setNewPassword(this.newPasswordForm.getRawValue())
      .then((res: any) => {
        this.inProgress = false;
        this.router.navigate(['/info/reset-completed']);
      })
      .catch(err => {
        this.inProgress = false;
        if (err.error.hasOwnProperty('errors')) {
          Object.assign(this.serverErrors, err.error.errors);
        } else {
          if (err.error.hasOwnProperty('message')) {
            this.newPasswordError = err.error.message;
          }
        }
      })
  }

  /**
   * Set focus to element
   * @param {string} id
   */
  setFocus(id: string) {
    document.getElementById(id).focus();
  }

  get password()        { return this.newPasswordForm.get('password'); }
  get passwordConfirm() { return this.newPasswordForm.get('passwordConfirm'); }
  get token()           { return this.newPasswordForm.get('token'); }
}
