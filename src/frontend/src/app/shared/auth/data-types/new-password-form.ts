export interface newPasswordForm {
  password: string,
  passwordConfirm: string,
  token: string
}
