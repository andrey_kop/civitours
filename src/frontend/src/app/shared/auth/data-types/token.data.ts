export interface TokenUserData {
  userId: number;
  userName: string;
  isAdmin: boolean;
}

export interface TokenData {
  data: TokenUserData,
  exp: number,
  iat: number,
  iss: string,
  nbf: number
}


