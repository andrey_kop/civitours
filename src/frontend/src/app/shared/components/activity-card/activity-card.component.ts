import { Component, Input, OnInit } from '@angular/core';
import { Activity } from '@shared/backend/data-types/activity.types';
import { BookingCalendarService } from '@shared/booking-calendar/booking-calendar.service';

@Component({
  selector: 'app-activity-card',
  templateUrl: './activity-card.component.html',
  styleUrls: ['./activity-card.component.styl']
})
export class ActivityCardComponent implements OnInit {

  @Input() activity: Activity;
  @Input() isHorizontal = false;

  constructor(private calendarService: BookingCalendarService) { }

  ngOnInit() {
  }

  /**
   * Emit new clicked event
   */
  click() {
    this.calendarService.openActivity(this.activity.id);
  }

}
