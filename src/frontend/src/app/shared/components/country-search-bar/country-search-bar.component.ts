import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Country } from '@shared/backend/data-types/geo.types';

@Component({
  selector: 'app-country-search-bar',
  templateUrl: './country-search-bar.component.html',
  styleUrls: ['./country-search-bar.component.styl']
})
export class CountrySearchBarComponent implements OnInit {

  @Output() onCountrySelected = new  EventEmitter<string>();
  @Input() selectedCountry: string;
  selectedName: string;

  isOpened = false;
  countriesSource: Country[] = [];
  countries: Country[] = [];

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.rest.getDestinationCountryList().then(countries => {
      this.countries = countries.slice(0, 10);
      this.countriesSource = countries;
      for (let i=0; i < countries.length; i++) {
        if ((this.selectedCountry === countries[i].code) || (this.selectedCountry == String(countries[i].id))) {
          this.selectedName = countries[i].name;
          this.selectedCountry = countries[i].code;
          break;
        }
      }
    });
  }

  public selectCountry(code: string, name: string) {
    this.isOpened = false;
    this.selectedCountry = code;
    this.selectedName = name;
    this.onCountrySelected.next(code);
  }

  public searchCountry(event: any) {
    const searchText = event.target.value;
    if (searchText) {
      this.countries = this.countriesSource.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1 ).slice(0, 10);
    } else {
      this.countries = this.countriesSource.slice(0, 10)
    }
  }

}
