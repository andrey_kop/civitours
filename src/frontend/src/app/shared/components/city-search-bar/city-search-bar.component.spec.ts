import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitySearchBarComponent } from './city-search-bar.component';

describe('CitySearchBarComponent', () => {
  let component: CitySearchBarComponent;
  let fixture: ComponentFixture<CitySearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitySearchBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitySearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
