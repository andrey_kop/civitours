import { NouiFormatter } from 'ng2-nouislider';

export class PriceFormatter implements NouiFormatter {
  to(value: number): string {
    if (0 == value) {
      return 'free';
    }
    return '€ ' +  value.toFixed();
  }

  from(value: string): number {
    if (-1 !== value.indexOf('free')) {
      return 0;
    }
    return Number(value.split(" ")[1]);
  }
}

export class DurationFormatter implements NouiFormatter {
  to(value: number): string {
    return value.toFixed() + ' hrs';
  }

  from(value: string): number {
    return Number(value.split(" ")[0]);
  }
}