import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Category } from '@shared/backend/data-types/activity.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivityFilter, AjaxActivityFilter, FastActivityFilter, timeTypes } from '../data-types/filter.types';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { DurationFormatter, PriceFormatter } from '@shared/components/activity-filter/tooltip.formatter';
import { NgbDatepicker, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-activity-filter',
  templateUrl: './activity-filter.component.html',
  styleUrls: ['./activity-filter.component.styl']
})
export class ActivityFilterComponent implements OnInit {

  categories: Promise<Category[]>;

  clearFlag = false;
  timeTypes = timeTypes;
  filter = new ActivityFilter();
  prices = [this.filter.minPrice, this.filter.maxPrice];
  durations = [this.filter.minDuration, this.filter.maxDuration];

  @ViewChild('datePicker') datePicker: NgbDatepicker;

  @Input() offset = 0;

  @Input() set selectedCategories(categories: number[]) {
    this.filter.categories = categories;
  }

  priceSliderConfig = {
    connect:  true,
    range: {
      min:      this.filter.minPrice,
      max:      this.filter.maxPrice,
    },
    step:     1,
    tooltips: [
      new PriceFormatter(),
      new PriceFormatter()
    ]
  };

  durationSliderConfig = {
    connect:  true,
    range: {
      min:      this.filter.minDuration,
      max:      this.filter.maxDuration,
    },
    step:     1,
    tooltips: [
      new DurationFormatter(),
      new DurationFormatter()
    ]
  };

  private fastFilter = new Subject<FastActivityFilter>();
  private ajaxFilter = new Subject<AjaxActivityFilter>();

  @Output() onFastFilter: Observable<FastActivityFilter>;
  @Output() onAjaxFilter: Observable<AjaxActivityFilter>;

  constructor(private rest: RestApiService) {
    // Don`t react until 300ms are passed
    this.onFastFilter = this.fastFilter.asObservable().debounceTime(300);
    this.onAjaxFilter = this.ajaxFilter.asObservable().debounceTime(500);
  }

  ngOnInit() {
    this.categories = this.rest.getActivityCategoriesList();
    this.updateFilter();
  }

  /**
   * callback for category select
   * @param {number} id
   */
  selectCategory(id: number) {
    if (this.filter.categories.includes(id)) {
      this.filter.categories = this.filter.categories.filter(item => item != id)
    } else {
      this.filter.categories.push(id);
    }
    this.updateFilter();
  }

  /**
   * callback for time type select
   * @param {number} id
   */
  selectTimeType(id: number) {
    if (this.filter.timeTypes.includes(id)) {
      this.filter.timeTypes = this.filter.timeTypes.filter(item => item != id)
    } else {
      this.filter.timeTypes.push(id);
    }
    this.updateFilter(true);
  }

  /**
   * callback for time select
   * @param date
   */
  selectDate(date) {
    this.filter.dates = [this.formatDate(date)];
    this.clearFlag = false;
    this.updateFilter(true);
  }

  /**
   * callback for time filter reset
   */
  resetTimeFilter() {
    this.filter.timeTypes = [];
    this.filter.dates = [];
    this.clearFlag = true;
    this.updateFilter(true)
  }

  /**
   * Callback for rate change
   * @param {number} value
   */
  changeRate(value: number) {
    this.filter.rate = value;
    this.updateFilter();
  }

  /**
   * callback for rate reset
   */
  resetRateFilter() {
    this.filter.rate = 0;
    this.updateFilter();
  }

  /**
   * callback for duration change
   */
  changeDuration() {
    this.filter.minDuration = this.durations[0];
    this.filter.maxDuration = this.durations[1];
    this.updateFilter();
  }

  /**
   * callback for price change
   */
  changePrice() {
    this.filter.minPrice = this.prices[0];
    this.filter.maxPrice = this.prices[1];
    this.updateFilter();
  }

  /**
   * Update corresponding filter and emit event
   * @param {boolean} isAjax
   */
  updateFilter(isAjax = false) {
    if (isAjax) {
      this.ajaxFilter.next(AjaxActivityFilter.fromActivityFilter(this.filter, this.offset));
    } else {
      this.fastFilter.next(FastActivityFilter.fromActivityFilter(this.filter));
    }
  }

  /**
   * Call back for date check
   * @param {NgbDateStruct} date
   * @returns {boolean}
   */
  isDisabled = (date: NgbDateStruct) => {
    const now = new Date();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    now.setMilliseconds(0);
    const cal = new Date(date.year, date.month - 1, date.day);
    return now > cal;
  };

  /**
   * Time format funciton
   *
   * @param {number} time
   * @returns {string}
   */
  formatTime(time: number): string {
    return this.pad(time, 2) + ':00';
  }

  /**
   * format date to correct string
   * @param {NgbDateStruct} date
   * @returns {string}
   */
  private formatDate(date: NgbDateStruct): string {
    return date.year + "-" + this.pad(date.month, 2) + "-" + this.pad(date.day, 2)
  }

  /**
   * Add zero num padding
   * @param {number} num
   * @param {number} size
   * @returns {string}
   */
  private pad(num: number, size: number):string {
    let s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }

}
