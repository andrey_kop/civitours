import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { HttpEventType } from '@angular/common/http';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.styl']
})
export class UploaderComponent implements OnInit {

  error: string = null;
  isUploading = false;
  uploadProgress = 0;
  uploadError = '';
  @Output() imageUploaded = new EventEmitter<UploadResult>();

  constructor(private rest: RestApiService) { }

  ngOnInit() {
  }

  /**
   * Handle upload image input
   * @param {FileList} files
   */
  handleFileInput(files: FileList) {
    let fileItem = files.item(0);
    this.isUploading = true;
    this.uploadError = '';
    this.rest.uploadImage(fileItem).subscribe(
      event => this.handleProgress(event),
      err => {
        this.isUploading = false;
        this.uploadError = err.error.message;
      }
    );
  }

  /**
   * Handle progress of image loading
   * @param event
   */
  handleProgress(event: any) {
    if (event.type === HttpEventType.UploadProgress) {
      this.uploadProgress = Math.round(100 * event.loaded / event.total);
    }

    if (event.type === HttpEventType.Response) {
      this.isUploading = false;
      this.imageUploaded.next(event.body);
    }
  }

}
