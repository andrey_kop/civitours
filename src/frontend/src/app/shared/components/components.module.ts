import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityCardComponent } from '@shared/components/activity-card/activity-card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FeaturedActivitiesComponent } from '@shared/components/featured-activities/featured-activities.component';
import { DestinationCardComponent } from '@shared/components/destination-card/destination-card.component';
import { DestinationCarouselComponent } from '@shared/components/destination-carousel/destination-carousel.component';
import { DragScrollModule } from 'ngx-drag-scroll';
import { RouterModule } from '@angular/router';
import { CategoryFilterComponent } from './category-filter/category-filter.component';
import { NouisliderModule } from 'ng2-nouislider';
import { FormsModule } from '@angular/forms';
import { ActivityFilterComponent } from './activity-filter/activity-filter.component';
import { CountrySearchBarComponent } from '@shared/components/country-search-bar/country-search-bar.component';
import { CitySearchBarComponent } from './city-search-bar/city-search-bar.component';
import { KeepHtmlPipe } from './pipes/keep-html.pipe';
import { UploaderComponent } from './uploader/uploader.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    DragScrollModule,
    FormsModule,
    NouisliderModule,
  ],
  declarations: [
    ActivityCardComponent,
    ActivityFilterComponent,
    FeaturedActivitiesComponent,
    DestinationCardComponent,
    DestinationCarouselComponent,
    CategoryFilterComponent,
    CountrySearchBarComponent,
    CitySearchBarComponent,
    KeepHtmlPipe,
    UploaderComponent
  ],
  exports: [
    ActivityCardComponent,
    ActivityFilterComponent,
    FeaturedActivitiesComponent,
    DestinationCardComponent,
    DestinationCarouselComponent,
    CategoryFilterComponent,
    CountrySearchBarComponent,
    CitySearchBarComponent,
    KeepHtmlPipe,
    UploaderComponent
  ]
})
export class ComponentsModule { }
