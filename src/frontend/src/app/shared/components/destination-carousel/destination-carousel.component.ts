import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DestinationCity } from '@shared/backend/data-types/destination.types';
import { DragScrollDirective } from 'ngx-drag-scroll';

@Component({
  selector: 'app-destination-carousel',
  templateUrl: './destination-carousel.component.html',
  styleUrls: ['./destination-carousel.component.styl']
})
export class DestinationCarouselComponent implements OnInit {

  @Input() showAllButton = true;
  @Input() header = "Main Destinations";
  @Input() headerClass = "destination__header";
  @Input() wideCards = false;
  @Input() destinations: DestinationCity[] = [];

  constructor() { }

  leftNavDisabled = false;
  rightNavDisabled = false;

  @ViewChild('scroll', {read: DragScrollDirective}) ds: DragScrollDirective;

  ngOnInit() {
    this.ds.scrollbarHidden = true;
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  leftBoundStat(reachesLeftBound: boolean) {
    this.leftNavDisabled = reachesLeftBound;
  }

  rightBoundStat(reachesRightBound: boolean) {
    this.rightNavDisabled = reachesRightBound;
  }
}
