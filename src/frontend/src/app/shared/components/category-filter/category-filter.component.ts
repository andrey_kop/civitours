import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Category } from '@shared/backend/data-types/activity.types';

@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.styl']
})
export class CategoryFilterComponent implements OnInit {

  categories: Promise<Category[]>;

  @Input() selectedCategories: number[] = [];
  @Output() onCategoryClicked = new  EventEmitter<number[]>();

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.categories = this.rest.getActivityCategoriesList();
  }

  /**
   * Category click callback
   *
   * @param {number} id
   */
  clickCategory(id: number) {
    if (this.selectedCategories.includes(id)) {
      this.selectedCategories = this.selectedCategories.filter(item => item != id)
    } else {
      this.selectedCategories.push(id);
    }
    this.onCategoryClicked.next(this.selectedCategories);
  }

  clear() {
    this.selectedCategories = [];
    this.onCategoryClicked.next(this.selectedCategories);
  }
}
