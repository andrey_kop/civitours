import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { ActivityOrder } from '@shared/backend/data-types/activity.types';

@Injectable()
export class BookingCalendarService {

  constructor() {}

  private activitySubject = new Subject<number>();
  private orderSubject = new Subject<ActivityOrder>();

  /**
   * Send request to open calendar for activity
   *
   * @param {number} activityId
   */
  openActivity(activityId: number) {
    this.activitySubject.next(activityId);
  }

  /**
   * Get activity observable
   * @returns {Observable<number>}
   */
  getActivityObservable(): Observable<number> {
    return this.activitySubject.asObservable();
  }

  /**
   * Place booked order
   *
   * @param {ActivityOrder} order
   */
  placeOrder(order: ActivityOrder) {
    this.orderSubject.next(order);
  }

  /**
   * get order observable
   * @returns {Observable<ActivityOrder>}
   */
  getOrderObservable(): Observable<ActivityOrder> {
    return this.orderSubject.asObservable();
  }
}
