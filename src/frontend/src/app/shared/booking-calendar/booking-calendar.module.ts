import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ScrollbarModule } from 'ngx-scrollbar';

import { BookingCalendarComponent } from './calendar/booking-calendar.component';
import { CalendarSliderComponent } from './calendar-slider/calendar-slider.component';
import { RelatedActivitiesComponent } from './related-activities/related-activities.component';
import { ComponentsModule } from '@shared/components/components.module';
import { BookingCalendarService } from '@shared/booking-calendar/booking-calendar.service';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    ComponentsModule,
    ScrollbarModule
  ],
  declarations: [
    BookingCalendarComponent,
    CalendarSliderComponent,
    RelatedActivitiesComponent
  ],
  entryComponents: [RelatedActivitiesComponent],
  exports: [
    BookingCalendarComponent,
    CalendarSliderComponent
  ],
  providers: [
    BookingCalendarService
  ]
})
export class BookingCalendarModule { }
