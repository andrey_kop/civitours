import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbAccordion, NgbDatepicker, NgbDateStruct, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs/Subject';
import {
  ActivityOrder,
  ActivitySchedule,
  ActivityTicket,
  ActivityType
} from '@shared/backend/data-types/activity.types';
import * as moment from 'moment';

@Component({
  selector: 'app-booking-calendar',
  templateUrl: './booking-calendar.component.html',
  styleUrls: ['./booking-calendar.component.styl']
})
export class BookingCalendarComponent implements OnInit {

  @Input() schedule: ActivitySchedule;
  @Input() types: ActivityType[];
  @Output() onBooked = new EventEmitter<ActivityOrder>();

  // Inner storage to speedup ticket data searching
  ticketHash:{[id: number]: ActivityTicket} = {};
  typesHash:{[id: number]: ActivityType} = {};
  objectKeys = Object.keys;

  tickets: ActivityTicket[] = [];

  selectedDate = '';
  order = new ActivityOrder();
  times: {time: number, name: string}[] = [];
  peopleHeader = 'People';
  isLoaded = false;
  inSummit = false;
  isOk = false;
  status = new Subject<boolean>();

  @ViewChild('datePicker') datePicker: NgbDatepicker;
  @ViewChild('typesAcc') typesAcc: NgbAccordion;
  @ViewChild('dateAcc') dateAcc: NgbAccordion;

  @ViewChild('dateTooltip') dateTooltip: NgbTooltip;
  @ViewChild('timeTooltip') timeTooltip: NgbTooltip;
  @ViewChild('ticketsTooltip') ticketsTooltip: NgbTooltip;


  /**
   * Constructor
   */
  constructor() {}

  /**
   * Before init hook
   */
  ngOnInit() {
    if(this.types.length > 0) {
      this.order.type = this.types[0].id;
      this.tickets = this.types[0].tickets;
      this.types.forEach(type => this.typesHash[type.id] = type);
      this.tickets.forEach(ticket => this.ticketHash[ticket.id] = ticket);
    }
  }

  /**
   * Add time to order
   * @param {string} time
   */
  selectTime(time: number) {
    this.order.time = time;
    this.timeTooltip.close();
  }

  /**
   * Select type of order
   * @param {string} type
   */
  selectType(type: ActivityType) {
    this.order.type = type.id;
    this.tickets = type.tickets;
    this.tickets.forEach(ticket => this.ticketHash[ticket.id] = ticket);
    this.typesAcc.toggle('types-panel');
  }

  /**
   * Add selected ticket to order
   * @param id
   */
  addTicket(id: number) {
    if (!(id in this.ticketHash)) {
      throw new Error('Wrong ticket code provided: ' + id);
    }
    if (!(id in this.order.tickets)) {
      this.order.tickets[id] = 0;
    }
    ++this.order.tickets[id];
    this.buildPeopleHeader();
  }

  /**
   * remove ticket from order
   * @param id
   */
  removeTicket(id: number) {
    if (!(id in this.ticketHash)) {
      throw new Error('Wrong ticket code provided: ' + id);
    }
    if (id in this.order.tickets) {
      if (this.order.tickets[id] <= 1) {
        delete this.order.tickets[id];
      } else {
        --this.order.tickets[id];
      }
    }
    this.buildPeopleHeader();
  }

  /**
   * Add date to order
   * @param {NgbDateStruct} date
   */
  selectDate(date: NgbDateStruct) {
    this.times = [];
    this.selectedDate = this.formatDate(date);
    const calendar = moment.parseZone(this.formatDate(date, this.schedule.offset), 'YYYY-MM-DD ZZ');
    this.schedule.time.forEach(time => {
      const schedule = moment.unix(time).utcOffset(this.schedule.offset);
      if (calendar.isSame(schedule, 'day')) {
        this.times.push({
          time: time,
          name: schedule.format('HH:mm')
        });
      }
    });
    this.dateAcc.toggle('date-panel');
    this.dateTooltip.close();
  }

  /**
   * Calculate total of order
   * @returns {number}
   */
  getTotal():number {
    let total = 0;
    for (let id in this.order.tickets) {
      total += this.ticketHash[id].price * this.order.tickets[id];
    }
    return total;
  }

  /**
   * check all data and submit
   */
  submit() {
    if(!this.order.time) {
      this.timeTooltip.open();
    }
    if(Object.keys(this.order.tickets).length == 0) {
      this.ticketsTooltip.open();
    }
    if (!this.order.isValid()) {
      return;
    }
    this.onBooked.emit(this.order);
  }

  /**
   * Call back for date check
   * @param {NgbDateStruct} date
   * @returns {boolean}
   */
  isDisabled = (date: NgbDateStruct) => {
    if (null === this.schedule.time) {
      return true; // No available dates
    }
    let calendar = moment.parseZone(this.formatDate(date, this.schedule.offset), 'YYYY-MM-DD ZZ');
    let now = moment().utcOffset(this.schedule.offset);
    if (calendar.isBefore(now, 'day')) {
      return true;
    }

    for (let index = 0; index < this.schedule.time.length; index++) {
      let schedule = moment.unix(this.schedule.time[index]).utcOffset(this.schedule.offset);
      if(calendar.isSame(schedule, 'day')) {
        return false; // Switch on included day
      }
    }
    return true;
  };

  /**
   * Rebuild header after ticket selection
   */
  private buildPeopleHeader() {
    if (!Object.keys(this.order.tickets).length) {
      this.peopleHeader = 'People';
      return;
    }
    let headerArray = [];
    for (let id in this.order.tickets) {
      headerArray.push(this.order.tickets[id] + ' ' + this.ticketHash[id].name);
    }
    this.peopleHeader = headerArray.join(', ');
    this.ticketsTooltip.close();
  }

  /**
   * format date to correct string
   * @param {NgbDateStruct} date
   * @param offset
   * @returns {string}
   */
  private formatDate(date: NgbDateStruct, offset: number = null): string {
    const str = date.year + "-" + this.pad(date.month, 2) + "-" + this.pad(date.day, 2);
    if (null === offset) {
      return str;
    }
    const offsetStr = (offset < 0 ? '-' : '+') + this.pad(offset, 2) + ':00';
    return str + ' ' + offsetStr;
  }

  /**
   * Add zero num padding
   * @param {number} num
   * @param {number} size
   * @returns {string}
   */
  private pad(num: number, size: number):string {
    let s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }
}
