import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Activity } from '@shared/backend/data-types/activity.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-related-activities',
  templateUrl: './related-activities.component.html',
  styleUrls: ['./related-activities.component.styl']
})
export class RelatedActivitiesComponent implements OnInit {

  relatedActivities: Promise<Activity[]>;

  @Input() set idActivity(id: number) {
    this.relatedActivities = this.rest.getRelatedActivities(id);
  }
  @Output() onFeatureCalendarClicked = new  EventEmitter<number>();

  constructor(public activeModal: NgbActiveModal, private rest: RestApiService) { }

  ngOnInit() {

  }

  onFeatureClicked(id) {
    this.onFeatureCalendarClicked.next(id);
  }

}
