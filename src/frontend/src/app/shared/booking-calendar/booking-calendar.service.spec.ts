import { TestBed, inject } from '@angular/core/testing';

import { BookingCalendarService } from './booking-calendar.service';

describe('BookingCalendarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookingCalendarService]
    });
  });

  it('should be created', inject([BookingCalendarService], (service: BookingCalendarService) => {
    expect(service).toBeTruthy();
  }));
});
