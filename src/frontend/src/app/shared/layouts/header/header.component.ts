import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.styl'],
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  classAddition = '';

  private static readonly listUnscrolled = [
    '/destinations',
    '/city',
    '/activity'
  ];


  constructor(router: Router) {
    router.events.subscribe(route => {
      if (route instanceof NavigationEnd) {
        if (-1 == route.url.indexOf('#')) {
          window.scroll(0,0);
        }
        const url = route.url.split('?')[0];
        if (url === '/') {
          this.classAddition = '';
          return;
        }
        for (let index = 0; index < HeaderComponent.listUnscrolled.length; index ++) {
          if (-1 !== url.indexOf(HeaderComponent.listUnscrolled[index])) {
            this.classAddition = '';
            return;
          }
        }
        this.classAddition = 'head-top--scroll';
      }
    });
  }
}
