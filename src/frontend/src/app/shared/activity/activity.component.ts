import { Component, Input, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity, ActivityFull, ActivityOrder } from '@shared/backend/data-types/activity.types';
import { AppConfig } from '@shared/config/app-config.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RelatedActivitiesComponent } from '@shared/booking-calendar/related-activities/related-activities.component';
import { BasketService } from '@shared/basket/basket.service';
import { BookingCalendarService } from '@shared/booking-calendar/booking-calendar.service';
import { Review } from '@shared/backend/data-types/review.types';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.styl']
})
export class ActivityComponent implements OnInit {

  @Input() forAdmin = false;
  activityInfo: ActivityFull;
  relatedActivities: Activity[] = [];
  reviews: Review[] = [];
  isLoading: boolean;

  relatedModal: NgbModalRef;
  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];

  constructor(private rest: RestApiService,
              private route: ActivatedRoute,
              private router: Router,
              private sanitizer: DomSanitizer,
              private basket: BasketService,
              private modalService: NgbModal,
              private calendarService: BookingCalendarService) {
    this.isLoading = true;
  }

  ngOnInit() {

    this.galleryOptions = [
      {
        width: '100%',
        height: '100%',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];

    this.route.fragment.subscribe(fragment => {
      let el = document.getElementById(fragment);
      if(null !== el) {
        el.scrollIntoView();
      }
    });

    this.route.params.subscribe(params => {
      if(params.hasOwnProperty('id')) {
        this.isLoading = true;
        const activityInfo = this.rest.getActivityFull(params.id, this.forAdmin).then(activity => {
          this.activityInfo = activity;
          this.activityInfo.meeting_point_longitude = parseFloat(this.activityInfo.meeting_point_longitude.toString());
          this.activityInfo.meeting_point_latitude = parseFloat(this.activityInfo.meeting_point_latitude.toString());
          this.galleryImages = [];
          activity.gallery.forEach(galleryItem => {
            this.galleryImages.push({
              small: galleryItem.path,
              medium: galleryItem.path,
              big: galleryItem.path,
              description: galleryItem.description
            })
          })
        });

        const relatedActivities = this.rest.getRelatedActivities(params.id).then(activities => this.relatedActivities = activities);
        const reviewsInfo = this.rest.getReviewsForActivity(params.id).then(reviews => this.reviews = reviews);

        //Wait until all data has been loaded
        Promise.all([
          activityInfo,
          relatedActivities,
          reviewsInfo
        ]).then(() => this.isLoading = false);
      }
    });
  }

  /**
   * Perform add order operation
   * @param {ActivityOrder} order
   */
  book(order: ActivityOrder) {
    this.basket.addItem(this.activityInfo, order);
    this.calendarService.placeOrder(order);
    if(this.relatedModal) {
      this.relatedModal.close();
    }
    this.relatedModal = this.modalService.open(RelatedActivitiesComponent, {
      windowClass: 'modal--added'
    });
    this.relatedModal.componentInstance.idActivity = this.activityInfo.id;
  }

  navigateToFragment(fragment) {
    this.router.navigate([], {fragment: fragment});
  }

}
