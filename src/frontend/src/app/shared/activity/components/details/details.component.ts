import { Component, Input, OnInit } from '@angular/core';
import { ActivityFull } from '@shared/backend/data-types/activity.types';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.styl']
})
export class DetailsComponent implements OnInit {

  @Input() activityInfo: ActivityFull;

  constructor() { }

  ngOnInit() {
  }

}
