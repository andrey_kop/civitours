import { Component, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { ActivityFull } from '@shared/backend/data-types/activity.types';
import { City } from '@shared/backend/data-types/geo.types';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.styl']
})
export class HeroComponent implements OnInit {

  @Input() activity: ActivityFull;
  selectedCountry: string;
  cityCode: number;
  countryCode: string;

  @ViewChild('cityPopover') public cityPopover: NgbPopover;
  @ViewChild('countryPopover') public countryPopover: NgbPopover;

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if('a' !== targetElement.tagName.toLowerCase()) {
      this.cityPopover.close();
      this.countryPopover.close();
    }
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

  countryChanged(country: string) {
    this.selectedCountry = country;
    this.countryCode = country;
    this.countryPopover.open();
  }

  cityChanged(city: City) {
    this.router.navigate([`/city/${city.id}`]);
  }

}
