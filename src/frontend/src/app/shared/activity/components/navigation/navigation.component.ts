import { AfterViewInit, Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.styl']
})
export class NavigationComponent implements AfterViewInit {

  readonly items = [
    {id: 'desc',    name: 'Description'},
    {id: 'prices',  name: 'Prices'},
    {id: 'details', name: 'Details'},
    {id: 'meeting', name: 'Meeting Point'},
    {id: 'cancel',  name: 'Cancellation'},
    {id: 'review',  name: 'Reviews'}
  ];

  selectedItem = this.items[0];

  @Output() onMenuChanged = new EventEmitter<string>();

  constructor() { }

  ngAfterViewInit() {
    this.onMenuChanged.next(this.selectedItem.id);
  }

  clickMenu(item) {
    this.selectedItem = item;
    this.onMenuChanged.next(item.id);
  }

}
