import { Injectable } from '@angular/core';
import { BaskedStoredItem } from '@shared/basket/data-types/basket-item';

@Injectable()
export abstract class StorageService {

  /**
   * Save items to storage
   *
   * @param {number} userId
   * @param {BaskedStoredItem[]} items
   * @returns {boolean}
   */
  public abstract saveItems(userId: number, items: BaskedStoredItem[]): boolean;

  /**
   * Load items from storage
   *
   * @param {number} userId
   * @returns {BaskedStoredItem[]}
   */
  public abstract loadItems(userId: number): BaskedStoredItem[];

}
