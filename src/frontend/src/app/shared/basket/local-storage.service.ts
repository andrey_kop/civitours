import { Injectable } from '@angular/core';
import { BaskedStoredItem } from '@shared/basket/data-types/basket-item';
import { StorageService } from '@shared/basket/storage.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class LocalStorageService extends StorageService {

  private static readonly STORAGE_KEY = 'basket';

  /**
   * Build storage key
   *
   * @param {number} userId
   * @returns {string}
   */
  private static buildStorageKey(userId: number) {
    return LocalStorageService.STORAGE_KEY + userId;
  }

  /**
   * @inheritDoc
   */
  public saveItems(userId: number, items: BaskedStoredItem[]): boolean {
    if (environment.isServer) {
      return false;
    }
    localStorage.setItem(LocalStorageService.buildStorageKey(userId), JSON.stringify(items));
  };

  /**
   * @inheritDoc
   */
  public loadItems(userId: number): BaskedStoredItem[] {
    if (environment.isServer) {
      return [];
    }
    const data = localStorage.getItem(LocalStorageService.buildStorageKey(userId));
    if (!data) {
      return [];
    }
    return JSON.parse(data);
  };
}
