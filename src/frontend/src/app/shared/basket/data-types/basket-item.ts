import { ActivityFull, ActivityOrder, ActivityTicket } from '@shared/backend/data-types/activity.types';

/**
 * Item of bascket. Immutable.
 */
export class BasketItem {

  /**
   * Cached result of ticket data
   *
   * @type BasketTicketData
   */
  private ticketDataCache: BasketTicketData = null;

  constructor(public id, public activity: ActivityFull, public order: ActivityOrder) {}

  /**
   * Retrieve info data for item
   *
   * @returns {BasketTicketData}
   */
  public getTicketData(): BasketTicketData {
    if (!this.ticketDataCache) {
      this.ticketDataCache = {ticketNames: [], total: 0};
      for(const code in this.order.tickets) {
        const info = this.getTicketInfo(parseInt(code), this.order.type);
        if (info) {
          this.ticketDataCache.total += this.order.tickets[code] * info.price;
          this.ticketDataCache.ticketNames.push(this.order.tickets[code] + " " + info.name);
        }
      }
    }
    return this.ticketDataCache;
  }

  /**
   * Get ticket information by code
   *
   * @param {string} ticketCode
   * @param ticketType
   */
  private getTicketInfo(ticketCode: number, ticketType: number): ActivityTicket {
    for(let index = 0; index < this.activity.types.length; index++) {
      if (this.activity.types[index].id === ticketType) {
        for (let index2 = 0; index2 < this.activity.types[index].tickets.length; index2++) {
          if (this.activity.types[index].tickets[index2].id === ticketCode) {
            return this.activity.types[index].tickets[index2]
          }
        }
      }
    }
    return null;
  }

  /**
   * Convert data for storage
   */
  public toStorage(): BaskedStoredItem {
    return {
      activityId: this.activity.id,
      order: this.order
    }
  }

}

export interface BasketTicketData {
  ticketNames: string[];
  total: number;
}

export interface BaskedStoredItem {
  activityId: number,
  order: ActivityOrder
}
