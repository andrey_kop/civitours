import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { BasketService } from '@shared/basket/basket.service';
import { BasketItem } from '@shared/basket/data-types/basket-item';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.styl']
})
export class BasketComponent implements OnInit {

  isOpened = false;
  items: BasketItem[];

  constructor(private basket: BasketService) { }

  @ViewChild('hostTarget') hostTarget;

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if(!this.hostTarget.nativeElement.contains(targetElement)) {
      // Clicked outside of basket
      if(!targetElement.hasAttribute('close-basket-item')) {
        // this is not delete item element
        this.isOpened = false;
      }

    }
  }

  ngOnInit() {
    this.basket.getBasketObservable().subscribe(items => this.items = items);
  }

  getTotal() {
    let total = 0;
    this.items.forEach(item => total += item.getTicketData().total);
    return total;
  }

  deleteItem(id: number) {
    this.basket.removeItem(id);
  }

}
