import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BaskedStoredItem, BasketItem } from '@shared/basket/data-types/basket-item';
import { Observable } from 'rxjs/Observable';
import { ActivityFull, ActivityOrder } from '@shared/backend/data-types/activity.types';
import { AuthService } from '@shared/auth/auth.service';
import { RestApiService } from '@shared/backend/rest-api.service';
import { StorageService } from '@shared/basket/storage.service';


@Injectable()
export class BasketService {

  private static readonly MAX_BASKET_SIZE = 100;

  private basketSubject: BehaviorSubject<BasketItem[]>;
  private basket = new Map<number, BasketItem>();

  constructor(private auth: AuthService, private rest: RestApiService, private storage: StorageService) {
    this.basketSubject= new BehaviorSubject<BasketItem[]>([]);
    this.auth.authStatusChange().subscribe(() => {
      this.loadFromStorage();
    });
  }

  /**
   * Load data from storage if available
   */
  private loadFromStorage() {
    let userId = 0;
    if (this.auth.isAuthenticated()) {
      userId = this.auth.getTokenData().userId;
    }
    let items = this.storage.loadItems(userId);
    let restPromises = [];
    this.basket.clear();
    let index = 0;
    items.forEach(item => restPromises.push(this.fullFillActivity.bind(this, index++)(item)));
    Promise.all(restPromises).then((result) => {
      result.forEach(item => this.basket.set(item.id, item));
      this.basketSubject.next(result);
    });
  }

  /**
   * Convert stored basket item to normal
   *
   * @param index
   * @param {BaskedStoredItem} item
   * @returns {Promise<BasketItem>}
   */
  private async fullFillActivity(index: number, item: BaskedStoredItem): Promise<BasketItem> {
    let activity = await this.rest.getActivityFull(item.activityId);
    return new BasketItem(index, activity, item.order);
  }

  /**
   * Save data to storage
   */
  private saveToStorage() {
    let userId = 0;
    if (this.auth.isAuthenticated()) {
      userId = this.auth.getTokenData().userId;
    }
    const items = this.getBasketCurrent();
    let itemsToStore = [];
    items.forEach(item => {
      itemsToStore.push(item.toStorage())
    });
    this.storage.saveItems(userId, itemsToStore);
  }

  /**
   * Update subject value
   */
  private updateSubject() {
    const iterator = this.basket.values();
    const items = [];
    while(true) {
      const result = iterator.next();
      if (result.done) break;
      items.push(result.value);
    }
    this.basketSubject.next(items);
  }

  /**
   * Get current basket items
   * @returns {BasketItem[]}
   */
  public getBasketCurrent(): BasketItem[] {
    return this.basketSubject.getValue();
  }

  /**
   * Get observable to subscribe basket changes
   * @returns {Observable<BasketItem[]>}
   */
  public getBasketObservable(): Observable<BasketItem[]> {
    return this.basketSubject.asObservable();
  }

  /**
   * Add item to basket
   *
   * @param {ActivityFull} activity
   * @param {ActivityOrder} order
   * @returns {boolean}
   */
  public addItem(activity: ActivityFull, order: ActivityOrder): boolean {
    let index = 0;
    while (true) {
      if (!this.basket.has(index)) {
        const basketItem = new BasketItem(index, activity, order);
        this.basket.set(index, basketItem);
        this.updateSubject();
        this.saveToStorage();
        break;
      }
      if (index++ > BasketService.MAX_BASKET_SIZE) {
        throw new RangeError("Maximum basket size is exceeded");
      }
    }
    return true;
  }

  /**
   * Remove item from basket
   *
   * @param {number} id
   * @returns {boolean}
   */
  public removeItem(id: number): boolean {
    const result = this.basket.delete(id);
    if (result) {
      this.updateSubject();
      this.saveToStorage();
    }
    return result;
  }
}
