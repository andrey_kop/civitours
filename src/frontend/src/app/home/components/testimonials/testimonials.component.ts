import { Component, OnInit } from '@angular/core';
import { VgAPI } from 'videogular2/core';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.styl']
})
export class TestimonialsComponent implements OnInit {

  api: VgAPI = null;
  isPlayed = true;
  isDirty = false;

  constructor() { }

  ngOnInit() {
  }

  play() {
    this.isPlayed = true;
    this.isDirty = true;
    this.api.play();
  }

  onPlayerReady(api: VgAPI) {
    this.isPlayed = false;
    this.api = api;
    this.api.getDefaultMedia().subscriptions.ended.subscribe(
      () => {
        // Set the video to the beginning
        this.api.getDefaultMedia().currentTime = 0;
        this.isPlayed = false;
      }
    );
  }

  videoClick() {
    if (this.isDirty) {
      this.isPlayed = !this.isPlayed;

      if (this.isPlayed) {
        this.api.play();
      } else {
        this.api.pause();
      }
    }
  }

}
