import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { BookingCalendarModule } from '@shared/booking-calendar/booking-calendar.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VgCoreModule } from 'videogular2/core'

import { HomeRoutes } from './home.routing';
import { HomeComponent } from './home.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { ComponentsModule } from '@shared/components/components.module';
import { BannerComponent } from './components/banner/banner.component';
import { HeroComponent } from './components/hero/hero.component';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutes,
    TranslateModule.forChild(),
    BookingCalendarModule,
    ComponentsModule,
    NgbModule,
    VgCoreModule
  ],
  declarations: [
    HomeComponent,
    TestimonialsComponent,
    BannerComponent,
    HeroComponent
  ]
})
export class HomeModule { }
