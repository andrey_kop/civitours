import { Routes, RouterModule } from '@angular/router';
import { MetaGuard } from '@ngx-meta/core';

import { WrapperComponent } from '@shared/layouts/wrapper/wrapper.component';
import { AdminGuard } from '@shared/auth/auth-guard';

const routes: Routes = [
  {
    path: '', component: WrapperComponent, canActivateChild: [MetaGuard], children: [
      { path: '', loadChildren: './home/home.module#HomeModule' },
      { path: 'destinations', loadChildren: './destinations/destinations.module#DestinationsModule' },
      { path: 'city/:id', loadChildren: './city/city.module#CityModule' },
      { path: 'activity/:id', loadChildren: './activity-page/activity-page.module#ActivityPageModule' },
      { path: 'admin', canActivate: [AdminGuard], loadChildren: './admin/admin.module#AdminModule' },
      { path: 'signup', loadChildren: './registration/registration.module#RegistrationModule' },
      { path: 'info/:id', loadChildren: './info/info.module#InfoModule' },
      { path: 'policy', loadChildren: './policy/policy.module#PolicyModule' },
      { path: 'mock', loadChildren: './mock-server-browser/mock-server-browser.module#MockServerBrowserModule' },
      { path: 'back', loadChildren: './transfer-back/transfer-back.module#TransferBackModule' },
      { path: '**', loadChildren: './not-found/not-found.module#NotFoundModule' },
    ]
  }
];
// must use {initialNavigation: 'enabled'}) - for one load page, without reload
export const AppRoutes = RouterModule.forRoot(routes, { initialNavigation: 'enabled' });
