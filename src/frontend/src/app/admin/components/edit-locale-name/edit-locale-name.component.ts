import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Country, Language, LocaleNames } from '@shared/backend/data-types/geo.types';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-edit-locale-name',
  templateUrl: './edit-locale-name.component.html',
  styleUrls: ['./edit-locale-name.component.styl']
})
export class EditLocaleNameComponent implements OnInit {

  @Input() languages: Language[] = [];

  localeNamesValue: {locale: any, name: string} [] = [];

  set localeNames(localeNames: LocaleNames) {
    this.localeNamesValue = [];
    for(let locale in localeNames) {
      this.localeNamesValue.push({
        locale: locale,
        name: localeNames[locale]
      })
    }
  }

  @Input() get localeNames() {
    let result = {};
    this.localeNamesValue.forEach(item => {
      if (item.locale.hasOwnProperty('code')) {
        item.locale = item.locale.code;
      }
      result[item.locale] = item.name
    });
    return result;
  }


  @Output()
  localeNamesChange = new EventEmitter<LocaleNames>();

  constructor() { }

  ngOnInit() {
  }

  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => this.languages.filter(
        v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));

  resultFormatter = (result: Language) => {
    return result.name + `( ${result.native_name})`;
  };

  inputFormatter = (result: any) => {
    if (result.hasOwnProperty('code')){
      return result.code;
    } else {
      return result;
    }
  };

  removeItem(index) {
    this.localeNamesValue.splice(index, 1);
    this.localeNamesChange.next(this.localeNames);
  }

  addItem() {
    this.localeNamesValue.push({locale: '', name: ''});
  }

  selectItem(index: number, item: Language) {
    this.localeNamesValue[index].locale = item.code;
    this.localeNamesChange.next(this.localeNames);
  }

  onInputChange() {
    this.localeNamesChange.next(this.localeNames);
  }

}
