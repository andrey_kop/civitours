import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLocaleNameComponent } from './edit-locale-name.component';

describe('EditLocaleNameComponent', () => {
  let component: EditLocaleNameComponent;
  let fixture: ComponentFixture<EditLocaleNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLocaleNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLocaleNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
