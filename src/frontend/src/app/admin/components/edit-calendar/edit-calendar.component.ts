import { Component, Input, OnInit } from '@angular/core';
import { ActivitySchedule } from '@shared/backend/data-types/activity.types';
import { NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-calendar',
  templateUrl: './edit-calendar.component.html',
  styleUrls: ['./edit-calendar.component.styl']
})
export class EditCalendarComponent implements OnInit {

  dayTimes: NgbTimeStruct[] = [];

  scheduleValue: ActivitySchedule;
  selectedDate: NgbDateStruct = null;
  private unixStartOfDay: number = null;
  private unixEndOfDay: number = null;

  set schedule(schedule: ActivitySchedule) {
    this.scheduleValue = schedule;
  }

  @Input() get schedule() {
    return this.scheduleValue;
  }

  constructor() { }

  ngOnInit() {
  }

  selectDate(date: NgbDateStruct) {
    this.selectedDate = date;
    const calendar = moment.parseZone(this.formatDate(date, this.scheduleValue.offset), 'YYYY-MM-DD ZZ');
    this.unixStartOfDay = calendar.startOf('day').unix();
    this.unixEndOfDay = calendar.endOf('day').unix();
    const filteredDates = this.scheduleValue.time.filter(val => val >= this.unixStartOfDay && val <= this.unixEndOfDay);
    this.dayTimes = [];
    filteredDates.forEach(timestamp => {
      let day = moment.unix(timestamp).utcOffset(this.scheduleValue.offset);
      this.dayTimes.push({
        hour: day.get('hours'),
        minute: day.get('minutes'),
        second: 0
      });
    });
  }

  /**
   * Perform sync between dates block and schedule model
   */
  syncDates() {
    //remove old dates for day
    this.scheduleValue.time = this.scheduleValue.time.filter(val => val < this.unixStartOfDay || val > this.unixEndOfDay);

    //fill the dates from array
    this.dayTimes.forEach(dayTime => {
      const timestamp = this.unixStartOfDay + dayTime.hour * 3600 + dayTime.minute * 60 + dayTime.second;
      this.scheduleValue.time.push(timestamp);
    });

  }

  /**
   * Remove date from list
   * @param {number} index
   */
  removeDate(index: number) {
    this.dayTimes.splice(index, 1);
    this.syncDates();
  }

  /**
   * Add new empty date
   */
  addDate() {
    this.dayTimes.push({
      hour: 12,
      minute: 0,
      second: 0
    });
    this.syncDates();
  }

  /**
   * format date to correct string
   * @param {NgbDateStruct} date
   * @param offset
   * @returns {string}
   */
  formatDate(date: NgbDateStruct, offset: number = null): string {
    const str = date.year + "-" + this.pad(date.month, 2) + "-" + this.pad(date.day, 2);
    if (null === offset) {
      return str;
    }
    const offsetStr = (offset < 0 ? '-' : '+') + this.pad(offset, 2) + ':00';
    return str + ' ' + offsetStr;
  }

  /**
   * Add zero num padding
   * @param {number} num
   * @param {number} size
   * @returns {string}
   */
  private pad(num: number, size: number):string {
    let s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }
}
