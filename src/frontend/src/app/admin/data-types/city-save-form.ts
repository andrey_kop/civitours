import { City, LocaleNames } from '@shared/backend/data-types/geo.types';

export class CitySaveForm {

  constructor(public name: string, public localeNames: LocaleNames, public imageFile: string = '') {}

  public static FromCity(city: City) {
    return new CitySaveForm(city.name, city.locale_name);
  }
}