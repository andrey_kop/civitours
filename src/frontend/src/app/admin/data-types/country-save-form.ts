import { Country, LocaleNames } from '@shared/backend/data-types/geo.types';

export class CountrySaveForm {

  constructor(public name: string, public localeNames: LocaleNames, public imageFile: string = '') {}

  public static FromCountry(country: Country) {
    return new CountrySaveForm(country.name, country.locale_name);
  }
}
