import { ActivityFull, ActivitySchedule, ActivityType, GalleryItem } from '@shared/backend/data-types/activity.types';

export class ActivityForm {
  constructor(
    public name = '',
    public avatar = '',
    public country = 0,
    public city = 0,
    public category = 0,
    public shortDescription = '',
    public description = '',
    public duration = '',
    public language = '',
    public included = '',
    public notIncluded = '',
    public whenToBook = '',
    public accessibility = '',
    public ticket = '',
    public howToBook = '',
    public meetingPointLatitude = 41.3895201, // Default to Barselona
    public meetingPointLongitude = 2.1691647, // Default to Barselona
    public meetingPointText = '',
    public cancelationHoursBefore = 0,
    public cancelationDescription = '',
    public types: ActivityType[] = [],
    public gallery: GalleryItemEdit[] = [],
    public schedule: ActivitySchedule = {time: [], offset: 0}
  ) {}

  public fillFromActivity(activity: ActivityFull) {
    this.name = activity.name;
    this.city = activity.city;
    this.country = activity.country;
    this.category = activity.category;
    this.shortDescription = activity.short_description;
    this.description = activity.description;
    this.duration = activity.duration;
    this.language = activity.language;
    this.included = activity.included;
    this.notIncluded = activity.not_included;
    this.whenToBook = activity.when_to_book;
    this.accessibility = activity.acessibility;
    this.ticket = activity.ticket;
    this.howToBook = activity.how_to_book;
    this.meetingPointLatitude = parseFloat(activity.meeting_point_latitude.toString()); // For google maps component
    this.meetingPointLongitude = parseFloat(activity.meeting_point_longitude.toString()); // For google maps component
    this.meetingPointText = activity.meeting_point_text;
    this.cancelationHoursBefore = activity.cancelation_hours_before;
    this.cancelationDescription = activity.cancelation_descriprion;
    this.types = activity.types;
    this.gallery = [];
    activity.gallery.forEach(item => this.gallery.push(new GalleryItemEdit(item)));
    this.schedule = activity.schedule;
  }
}

export class GalleryItemEdit implements GalleryItem {
  path: string;
  description: string;
  is_main: boolean;

  constructor(item: GalleryItem, public upload: string = '') {
    this.path = item.path;
    this.description = item.description;
    this.is_main = item.is_main;
  }
}
