import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminRoutes } from './admin.routing';
import { GeoEditComponent } from './pages/geo-edit/geo-edit.component';
import { ActivitiesComponent } from './pages/activities/activities.component';
import { EditCountryComponent } from './components/edit-country/edit-country.component';
import { EditCityComponent } from './components/edit-city/edit-city.component';
import { EditLocaleNameComponent } from './components/edit-locale-name/edit-locale-name.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ActivityEditComponent } from './pages/activity-edit/activity-edit.component';
import { ActivityViewComponent } from './pages/activity-view/activity-view.component';
import { ComponentsModule } from '@shared/components/components.module';
import { EditPricesComponent } from './components/edit-prices/edit-prices.component';
import { EditGalleryComponent } from './components/edit-gallery/edit-gallery.component';
import { AgmCoreModule } from '@agm/core';
import { AppConfig } from '@shared/config/app-config.service';
import { EditCalendarComponent } from './components/edit-calendar/edit-calendar.component';
import { ActivityModule } from '@shared/activity/activity.module';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutes,
    FormsModule,
    NgbModule,
    ActivityModule,
    ComponentsModule,
    AgmCoreModule.forRoot({
      apiKey: AppConfig.getConfiguration().maps.google
    })
  ],
  declarations: [
    AdminComponent,
    GeoEditComponent,
    ActivitiesComponent,
    EditCountryComponent,
    EditCityComponent,
    EditLocaleNameComponent,
    ActivityEditComponent,
    ActivityViewComponent,
    EditPricesComponent,
    EditGalleryComponent,
    EditCalendarComponent
  ]
})
export class AdminModule { }
