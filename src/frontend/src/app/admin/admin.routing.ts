import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { GeoEditComponent } from './pages/geo-edit/geo-edit.component';
import { ActivitiesComponent } from './pages/activities/activities.component';
import { ActivityEditComponent } from './pages/activity-edit/activity-edit.component';
import { ActivityViewComponent } from './pages/activity-view/activity-view.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Admin area'
      }
    },
    children: [
      {path: '',           redirectTo: 'geo'},
      {path: 'geo',        component: GeoEditComponent,     data: { meta: { title: 'Admin - Manage geo locations'}}},
      {path: 'activities/create',   component: ActivityEditComponent,  data: { meta: { title: 'Admin - Manage activities - New'}}},
      {path: 'activities/:id/edit', component: ActivityEditComponent,  data: { meta: { title: 'Admin - Manage activities - Edit'}}},
      {path: 'activities/:id/view', component: ActivityViewComponent,  data: { meta: { title: 'Admin - Manage activities - View'}}},
      {path: 'activities', component: ActivitiesComponent,  data: { meta: { title: 'Admin - Manage activities'}}},
    ]
  }
];

export const AdminRoutes = RouterModule.forChild(routes);