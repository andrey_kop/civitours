import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { City, Country } from '@shared/backend/data-types/geo.types';

@Component({
  selector: 'app-geo-edit',
  templateUrl: './geo-edit.component.html',
  styleUrls: ['./geo-edit.component.styl']
})
export class GeoEditComponent implements OnInit {

  countriesList$: Promise<Country[]>;
  citiesList$: Promise<City[]>;

  selectedCountry: Country = null;
  selectedCity: City = null;

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.countriesList$ = this.rest.getDestinationCountryList(true);
  }

  selectCountry(country: Country) {
    this.selectedCountry = country;
    this.citiesList$ = this.rest.getDestinationCityList(country.id, true);
    this.selectedCity = null;
  }

  selectCity(city: City) {
    this.selectedCity = city;
  }

  reloadCountryList() {
    this.countriesList$ = this.rest.getDestinationCountryList(true);
  }

  reloadCityList() {
    this.citiesList$ = this.rest.getDestinationCityList(this.selectedCountry.id, true);
  }

}
