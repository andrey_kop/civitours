import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityFull, Category } from '@shared/backend/data-types/activity.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { City, Country } from '@shared/backend/data-types/geo.types';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { of } from 'rxjs/observable/of';
import { ActivityForm } from '../../data-types/activity-form';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { AgmMap } from '@agm/core';

@Component({
  selector: 'app-activity-edit',
  templateUrl: './activity-edit.component.html',
  styleUrls: ['./activity-edit.component.styl']
})
export class ActivityEditComponent implements OnInit {

  isLoading = false;
  isSaving = false;
  activityInfo: ActivityFull;
  countries$: Promise<Country[]>;
  categories$: Promise<Category[]>;

  selectedCountryName = '';
  selectedCityName = '';

  heroUploadData: UploadResult;

  form = new ActivityForm();
  serverErrors = new ActivityForm();
  activityError = '';

  @ViewChild('map') public map: AgmMap;

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.countries$ = this.rest.getCountryList();
    this.categories$ = this.rest.getActivityCategoriesList();

    this.route.params.subscribe(params => {
      if(params.hasOwnProperty('id')) {
        this.isLoading = true;
        const activityInfo = this.rest.getActivityFull(params.id, true).then(activity => {
          this.activityInfo = activity;
          this.form.fillFromActivity(activity);
          this.selectedCountryName = activity.country_name;
          this.selectedCityName = activity.city_name;
          // this.map.triggerResize(true);
        });
        //Wait until all data has been loaded
        Promise.all([
          activityInfo,
        ]).then(() => this.isLoading = false);
      }
    });
  }

  /**
   * Save activity callback
   */
  saveActivity() {
    this.isSaving = true;
    const errorHandler = err => {
      this.isSaving = false;
      if (err.error.hasOwnProperty('errors')) {
        Object.assign(this.serverErrors, err.error.errors);
        this.activityError = 'Fo';
      }
      this.activityError = err.error.message;
    };
    const successHandler = () => {
      this.isSaving = false;
      this.activityError = '';
      this.serverErrors = new ActivityForm();
      this.router.navigate(['/admin/activities']);
    };
    if (this.activityInfo) {
      this.rest.updateActivity(this.activityInfo.id, this.form).then(successHandler).catch(errorHandler);
    } else {
      this.rest.createActivity(this.form).then(successHandler).catch(errorHandler);
    }
  }

  /**
   * Google map click handler
   * @param event
   */
  onMapClicked(event) {
    this.form.meetingPointLatitude = event.coords.lat;
    this.form.meetingPointLongitude = event.coords.lng;
  }

  /**
   * Callback on hero upload update
   * @param {UploadResult} uploadData
   */
  onHeroAvatarUpdate(uploadData: UploadResult) {
    this.heroUploadData = uploadData;
    this.form.avatar = uploadData.imageName;
  }

  /**
   * Callback to filter countries
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any>}
   */
  searchCountry = (text$: Observable<string>) => {
    return combineLatest(fromPromise(this.countries$), text$.debounceTime(200).distinctUntilChanged())
      .map(data => {
        let countryList = data[0];
        let searchText = data[1];
        return countryList.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1).slice(0, 10);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  countryFormatter = (result: Country) => {
    return result.name;
  };

  /**
   * Country select handler
   * @param event
   */
  onSelectCountry = (event) => {
    const country: Country = event.item;
    this.form.country = country.id;
  };

  /**
   * Callback to filter cities
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any[]>}
   */
  searchCity = (text$: Observable<string>) => {
    return text$.debounceTime(200).distinctUntilChanged()
      .flatMap(searchText => {
        if (!this.form.country || !searchText.length) {
          return of([]);
        }
        return this.rest.searchCities(this.form.country, searchText);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  cityFormatter = (result: City) => {
    return result.name;
  };

  /**
   * City select handler
   * @param event
   */
  onSelectCity = (event) => {
    const city: City = event.item;
    this.form.city = city.id;
  };
}
