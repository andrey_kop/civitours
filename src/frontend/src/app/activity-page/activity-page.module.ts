import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityRoutes } from './activity-page.routing';
import { ActivityModule } from '@shared/activity/activity.module';

@NgModule({
  imports: [
    CommonModule,
    ActivityRoutes,
    ActivityModule
  ]
})
export class ActivityPageModule { }
