import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { DestinationCountry } from '@shared/backend/data-types/destination.types';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.styl']
})
export class DestinationsComponent implements OnInit {

  private static readonly DEFAULT_COUNTRY_CODE = 'PL'; //Default country code

  country = DestinationsComponent.DEFAULT_COUNTRY_CODE;
  destinationInfo: Promise<DestinationCountry>;
  isLoading = true;

  constructor(private rest: RestApiService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      if(params.has('c')) {
        this.country = params.get('c');
      }
      this.loadCountryInformation();
    })
  }

  /**
   * Reload destination information
   */
  private loadCountryInformation () {
    this.isLoading = true;
    this.destinationInfo = this.rest.getDestinationCountry(this.country).then(res => {
      this.isLoading = false;
      return res;
    });
  }

  selectCountry(code: string) {
    if (this.country !== code) {
      this.country = code;
      this.loadCountryInformation();
    }
  }
}
