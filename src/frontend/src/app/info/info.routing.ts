import { Routes, RouterModule } from '@angular/router';
import { InfoComponent } from './info.component';

const routes: Routes = [
  {
    path: '', component: InfoComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Info description'
      }
    },
  },
];

export const InfoRoutes = RouterModule.forChild(routes);