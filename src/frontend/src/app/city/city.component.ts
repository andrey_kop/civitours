import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { DestinationCity } from '@shared/backend/data-types/destination.types';
import { ActivatedRoute } from '@angular/router';
import { Activity } from '@shared/backend/data-types/activity.types';
import { Review } from '@shared/backend/data-types/review.types';
import { AjaxActivityFilter, FastActivityFilter } from '@shared/components/data-types/filter.types';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.styl']
})
export class CityComponent implements OnInit {

  destinationInfo: Promise<DestinationCity>;
  activitiesPromise: Promise<Activity[]>;
  activities: Activity[] = [];
  reviews: Review[] = [];
  isLoading: boolean;
  isActivityLoading = false;

  categoriesFilter = [];
  activityCategoriesFilter = [];

  fastFilter = new FastActivityFilter();

  private idCity: number;
  private searchString = '';

  constructor(private rest: RestApiService, private route: ActivatedRoute) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if(params.hasOwnProperty('id')) {
        this.idCity = params.id;
        this.destinationInfo = this.rest.getDestinationCity(params.id);
        this.activitiesPromise = this.rest.getActivitiesForCity(params.id).then(activities => {
          this.isActivityLoading = false;
          return this.activities = activities;
        });
        let reviewsInfo = this.rest.getReviewsForCity(params.id).then(reviews => this.reviews = reviews);
        Promise.all([
          this.destinationInfo,
          this.activitiesPromise,
          reviewsInfo
        ]).then(() => this.isLoading = false);
      }
    });
  }

  /**
   * Callback for categories filter change
   * @param {number[]} categories
   */
  changedCategoriesFilter(categories: number[]) {
    this.categoriesFilter = categories;
    this.fastFilter.categories = categories;
    this.fastFilterChanged(this.fastFilter);
  }

  /**
   * Search filtering function
   *
   * @returns {Promise<void>}
   * @param search
   */
  async searchInput(search: string) {
    this.searchString = search;
    this.fastFilterChanged(this.fastFilter);
  }

  /**
   * Fast filtering function
   * @param {FastActivityFilter} filter
   */
  async fastFilterChanged(filter: FastActivityFilter) {
    const activitiesSource = await this.activitiesPromise;
    this.activityCategoriesFilter = filter.categories;
    this.fastFilter = filter;
    this.activities = activitiesSource.filter(item => {
      const searchPassed = !this.searchString.length || (-1 !== item.name.toLowerCase().indexOf(this.searchString.toLowerCase()));
      const categoryPassed = !this.fastFilter.categories.length || this.fastFilter.categories.includes(item.category);
      const pricePassed = (item.min_price >= this.fastFilter.minPrice) && (item.min_price <= this.fastFilter.maxPrice);
      const duration = parseInt(item.duration) || 0;
      const durationPassed = (duration >= this.fastFilter.minDuration) && (duration <= this.fastFilter.maxDuration);
      const ratePassed = item.rate >= this.fastFilter.rate * 2;
      return categoryPassed && pricePassed && durationPassed && ratePassed && searchPassed;
    });
  }

  /**
   * Perform async filtering
   *
   * @param {AjaxActivityFilter} filter
   * @returns {Promise<void>}
   */
  async ajaxFilterChanged(filter: AjaxActivityFilter) {
    this.activities = [];
    this.isActivityLoading = true;
    this.activitiesPromise = this.rest.getActivitiesForCity(this.idCity, filter);
    await this.fastFilterChanged(this.fastFilter);
    this.isActivityLoading = false;
  }


}
