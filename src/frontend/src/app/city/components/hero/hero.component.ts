import { Component, Input, OnInit } from '@angular/core';
import { DestinationCity } from '@shared/backend/data-types/destination.types';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.styl']
})
export class HeroComponent implements OnInit {

  @Input() destinationCity: DestinationCity;

  constructor() { }

  ngOnInit() {
  }

}
