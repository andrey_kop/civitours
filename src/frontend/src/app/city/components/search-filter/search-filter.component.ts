import { Component, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.styl']
})
export class SearchFilterComponent implements OnInit {

  typed = new Subject<string>();

  @Output() onSearch: Observable<string>;

  constructor() {
    // Don`t react until 300ms are passed
    this.onSearch = this.typed.asObservable().debounceTime(300);
  }

  ngOnInit() {
  }

}
