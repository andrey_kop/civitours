import { Routes, RouterModule } from '@angular/router';
import { CityComponent } from './city.component';

const routes: Routes = [
  {
    path: '', component: CityComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Destinations'
      }
    },
  },
];

export const CityRoutes = RouterModule.forChild(routes);
